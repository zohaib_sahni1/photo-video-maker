package com.photo.video.maker.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.GridViewAdapter;
import com.photo.video.maker.adapters.RecyclerViewAdapter;
import com.photo.video.maker.adapters.SlidingViewAdapter;
import com.photo.video.maker.interfaces.CounterTextInterface;
import com.photo.video.maker.interfaces.SlidingRecyclerViewGetterInterface;
import com.photo.video.maker.model.ImagesModel;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.RecyclerTouchListener;
import com.photo.video.maker.utils.WrapContentLinearLayoutManager;


import java.util.ArrayList;

public class SelectImageFragment extends Fragment implements View.OnClickListener, View.OnTouchListener, SlidingRecyclerViewGetterInterface, CounterTextInterface {
    LinearLayout fbAdContainer;
    BillingHelper billingHelper;
    private ArrayList<ImagesModel> allImagesList = new ArrayList<>();
    GridView gridViewPhotos;
    RecyclerView recyclerView;
    boolean boolean_folder;
    ImageView backBtn, doneBtn, crossBtn;
    GridViewAdapter gridViewAdapter;
    public static ArrayList<String> data = new ArrayList<>();
    int positionFolder = 0;
    int int_position;
    FragmentManager fragmentManager;
    Context mContext;
    private SlidingViewAdapter slidingViewAdapter;
    ConstraintLayout parentLayout;
    LinearLayout slidingViewContainer;
    View slidingView;
    RelativeLayout headerLayout;
    ImageView arrowBtnHeader;
    @SuppressLint("StaticFieldLeak")
    public static RecyclerView sliderRecyclerView;
    float dy;
    private LinearLayout topLayout;
    private TextView counterTextView;
    boolean isUpMode = true;
    TextView emptyText;
    Uri uriForImages;
    Cursor cursorForImages;
    int columnIndexData, colomnIndexFolderName;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_image, container, false);
        initViews(view);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            try {
                findPathOfImages();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {

                Toast.makeText(mContext, "Cannot show images because permission is denied by the user!", Toast.LENGTH_SHORT).show();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (allImagesList != null && allImagesList.size() > 0) {
            gridViewAdapter = new GridViewAdapter(getActivity(), allImagesList, positionFolder, slidingViewAdapter, this);
            gridViewPhotos.setAdapter(gridViewAdapter);
            setListeners();
        } else {
            emptyText.setVisibility(View.VISIBLE);
            gridViewPhotos.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            slidingView.setVisibility(View.GONE);
        }
        return view;
    }

    public void initViews(View view) {
        fragmentManager = getActivity().getSupportFragmentManager();
        topLayout = view.findViewById(R.id.topLayout);
        counterTextView = view.findViewById(R.id.counterTextView);
        parentLayout = view.findViewById(R.id.parentLayout);
        slidingViewContainer = view.findViewById(R.id.slidingContainer);
        slidingView = view.findViewById(R.id.slidingLayout);
        sliderRecyclerView = view.findViewById(R.id.recyclerViewSlidingVew);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        sliderRecyclerView.setLayoutManager(layoutManager);
        sliderRecyclerView.setItemAnimator(new DefaultItemAnimator());
        headerLayout = view.findViewById(R.id.headerLayout);
        slidingView.setOnTouchListener(this);
        arrowBtnHeader = view.findViewById(R.id.arrowBtn);
        slidingViewContainer.setVisibility(View.VISIBLE);
        backBtn = view.findViewById(R.id.backBtn);
        doneBtn = view.findViewById(R.id.doneBtn);
        crossBtn = view.findViewById(R.id.crossBtn);
        gridViewPhotos = view.findViewById(R.id.gridViewPhotos);
        recyclerView = view.findViewById(R.id.recyclerView);
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        view.findViewById(R.id.crossBtn).setOnClickListener(this);
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        emptyText = view.findViewById(R.id.emptyText);
    }

    public void setListeners() {
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                positionFolder = position;
                if (gridViewAdapter == null && allImagesList != null && allImagesList.size() > 0) {
                    gridViewAdapter = new GridViewAdapter(getActivity(), allImagesList, position, slidingViewAdapter, SelectImageFragment.this);
                } else {
                    if (allImagesList != null && allImagesList.size() > 0) {
                        try {
                            gridViewAdapter.setModelList(allImagesList);
                            gridViewAdapter.setPositionFolder(positionFolder);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }
                    }
                }
                try {
                    if (allImagesList != null && allImagesList.size() > 0) {
                        try {
                            gridViewPhotos.setAdapter(gridViewAdapter);
                            gridViewAdapter.notifyDataSetChanged();
                        } catch (ArrayIndexOutOfBoundsException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onStart() {
        super.onStart();
        data.clear();
    }

    public void findPathOfImages() {
        try {
            int_position = 0;
            String absolutePathOfImage = null;
            uriForImages = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String[] projectionStrings = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
            final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
            cursorForImages = getActivity().getContentResolver().query(uriForImages, projectionStrings, null, null, orderBy + " DESC");
            columnIndexData = cursorForImages.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);//path's data
            colomnIndexFolderName = cursorForImages.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            while (cursorForImages.moveToNext()) {
                try {
                    absolutePathOfImage = cursorForImages.getString(columnIndexData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < allImagesList.size(); i++) {
                    if (allImagesList.get(i).getStr_folder().equals(cursorForImages.getString(colomnIndexFolderName))) {
                        boolean_folder = true;
                        int_position = i;
                        break;
                    } else {
                        boolean_folder = false;
                    }
                }
                if (boolean_folder) {
                    ArrayList<String> allPaths = new ArrayList<>(allImagesList.get(int_position).getAl_imagepath());
                    allPaths.add(absolutePathOfImage);
                    try {
                        allImagesList.get(int_position).setAl_imagepath(allPaths);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ArrayList<String> allPaths = new ArrayList<>();
                    try {
                        allPaths.add(absolutePathOfImage);
                        ImagesModel objectModel = new ImagesModel();
                        objectModel.setStr_folder(cursorForImages.getString(colomnIndexFolderName));
                        objectModel.setAl_imagepath(allPaths);
                        allImagesList.add(objectModel);
                        gridViewAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), allImagesList);
            recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(recyclerViewAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                try {
                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.doneBtn:
                if (data.size() < 3) {
                    Toast.makeText(getActivity(), "Please Select At least 3 images for making video!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    SelectedImagesFragment nextFragment = SelectedImagesFragment.getInstance(data, true);
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, nextFragment).addToBackStack(null).commitAllowingStateLoss();
                }
                break;
            case R.id.crossBtn:
                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
            case R.id.headerLayout:
                if (isUpMode) {
                    slidingView.setY((parentLayout.getHeight()) - headerLayout.getHeight());
                    arrowBtnHeader.setImageResource(R.drawable.ic_up_arrow_btn);
                    isUpMode = false;
                } else {
                    slidingView.setY(topLayout.getHeight() + gridViewPhotos.getHeight() + recyclerView.getHeight() + 45);
                    arrowBtnHeader.setImageResource(R.drawable.ic_down_arrow_new_btn);
                    isUpMode = true;
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                slidingView.setY(topLayout.getHeight() + gridViewPhotos.getHeight() + recyclerView.getHeight() + 45);
            }
        }, 2);
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dy = view.getY() - motionEvent.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                if ((motionEvent.getRawY() + dy) > topLayout.getHeight() + gridViewPhotos.getHeight() + recyclerView.getHeight() + 10 && (motionEvent.getRawY() + dy) < (parentLayout.getHeight()) - headerLayout.getHeight()) {
                    if ((motionEvent.getRawY() + dy) > parentLayout.getHeight() - (3 * headerLayout.getHeight())) {
                        arrowBtnHeader.setImageResource(R.drawable.ic_up_arrow_btn);
                        view.setY((parentLayout.getHeight()) - headerLayout.getHeight());
                    } else {
                        arrowBtnHeader.setImageResource(R.drawable.ic_down_arrow_new_btn);
                    }
                    view.setY(motionEvent.getRawY() + dy);
                }
                break;
            case MotionEvent.ACTION_UP:
                if ((motionEvent.getRawY() + dy) > parentLayout.getHeight() - (3 * headerLayout.getHeight())) {
                    view.setY((parentLayout.getHeight()) - headerLayout.getHeight());
                    isUpMode = false;
                } else {
                    view.setY(topLayout.getHeight() + gridViewPhotos.getHeight() + recyclerView.getHeight() + 45);
                    isUpMode = true;
                }
                break;
            default:
                return true;
        }
        return true;
    }

    @Override
    public RecyclerView getSlidingRecyclerView() {
        return sliderRecyclerView;
    }

    @Override
    public void setDataList(ArrayList<String> data) {
        data = data;
    }

    @Override
    public ArrayList<String> getDataList() {
        return data;
    }

    @Override
    public void setCount(int val) {
        counterTextView.setText(String.valueOf(val));
    }
}
