package com.photo.video.maker.model;

public class NewModel {
    private int itemCount=0;
    private String itemPath;

    public NewModel() {

    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemPath() {
        return itemPath;
    }

    public void setItemPath(String itemPath) {
        this.itemPath = itemPath;
    }
}
