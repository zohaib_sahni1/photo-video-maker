package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.photo.video.maker.R;
import com.photo.video.maker.model.MusicModel;


import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder> {
    private List<MusicModel> musicModelList;
    Context context;

    public MusicAdapter(List<MusicModel> musicModelList, Context context) {
        this.musicModelList = musicModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            MusicModel model = musicModelList.get(position);
            holder.name.setText(model.getName());
            holder.title.setText(model.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return musicModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView title;
        private ConstraintLayout parentLayout;

        MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            title = itemView.findViewById(R.id.title);
            parentLayout = itemView.findViewById(R.id.parentLayout);
        }
    }
}
