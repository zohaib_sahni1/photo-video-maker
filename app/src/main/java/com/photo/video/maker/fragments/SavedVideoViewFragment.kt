package com.photo.video.maker.fragments;

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.FileProvider
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.VideoView
import com.facebook.ads.AdSize
import com.google.android.gms.ads.AdView
import com.photo.video.maker.BuildConfig
import com.photo.video.maker.R
import com.photo.video.maker.dialogs.DeleteThisDialog
import com.photo.video.maker.interfaces.DialogDelToFragmentInteraface
import com.photo.video.maker.utils.BillingHelper
import com.photo.video.maker.utils.DeleteUtils
import com.photo.video.maker.utils.MyConstants
import com.photo.video.maker.utils.MyEssentials
import java.io.File

class SavedVideoViewFragment : Fragment(), DialogDelToFragmentInteraface, MediaPlayer.OnCompletionListener,
    MediaPlayer.OnPreparedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private var videoName: String? = null
    private var videoPath: String? = null
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var mContext: Context
    private lateinit var videoPlayerView: VideoView
    private lateinit var mediaController: MediaController
    private lateinit var billingHelper: BillingHelper
    private lateinit var mAdView: AdView
    private lateinit var fbAdView: com.facebook.ads.AdView
    private lateinit var fbAdContainer: LinearLayout
    private lateinit var bottomNavigationView: BottomNavigationView


    companion object {
        fun getInstance(path: String?, name: String?): SavedVideoViewFragment {
            val args = Bundle()
            args.putString(MyConstants.SAVED_VIDEO_VIEW_NAME_KEY, name)
            args.putString(MyConstants.SAVED_VIDEO_VIEW_PATH_KEY, path)
            val fragment = SavedVideoViewFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = activity!!

    }

    override fun onResume() {
        super.onResume()
        if (activity != null) {
            mContext = activity!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_saved_video_view, container, false)
        initViews(view)
        activity!!.title = videoName
        return view
    }

    private fun initViews(view: View) {
        bottomNavigationView = view.findViewById(R.id.bottomNavMenu)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        mFragmentManager = activity!!.supportFragmentManager
        videoPlayerView = view.findViewById(R.id.videoViewSaved) as VideoView
        mediaController = MediaController(mContext)
        billingHelper = BillingHelper(mContext)
        mAdView = view.findViewById(R.id.adView)
        fbAdView = com.facebook.ads.AdView(mContext, resources.getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50)
        fbAdContainer = view.findViewById(R.id.fbAdContainer)
        videoPath = arguments?.getString(MyConstants.SAVED_VIDEO_VIEW_PATH_KEY)
        videoName = arguments?.getString(MyConstants.SAVED_VIDEO_VIEW_NAME_KEY)
        if (videoName!!.equals("Video")) {
            bottomNavigationView.visibility = View.GONE
        }
        mediaController.setAnchorView(videoPlayerView)
        mediaController.setMediaPlayer(videoPlayerView)
        videoPlayerView.setVideoURI(Uri.parse(videoPath))
        videoPlayerView.setMediaController(mediaController)
        videoPlayerView.requestFocus()
        videoPlayerView.setOnCompletionListener(this)
        videoPlayerView.setOnPreparedListener(this)
        videoPlayerView.start()
        //mediaController.show(500)
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView)
        }
    }

    override fun onPrepared(mp: MediaPlayer) {
        try {
            if (!activity!!.isFinishing) {
                mediaController.show(100)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.deleteBtnMenu -> {
                val deleteDialog =
                    DeleteThisDialog(mContext, activity!!, "This video will be deleted fom creations folder?", this)
                deleteDialog.show()
            }
            R.id.shareBtnMenu -> {
                try {
                    val uriForFile = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID, File(videoPath))
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "video/*"
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uriForFile)
                    startActivity(Intent.createChooser(shareIntent, "Share Using"))
                } catch (e: Exception) {
                }

            }
        }
        return true
    }

    override fun deleteThisImageOrVideo(boolean: Boolean) {
        val file =
            File(Environment.getExternalStorageDirectory().toString() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Videos" + File.separator)
        if (boolean) {
            DeleteUtils.deleteSingleFile(file, videoName)
        }
    }

    override fun onCompletion(mp: MediaPlayer?) {
        videoPlayerView.start()
    }

    override fun onPause() {
        super.onPause()
        videoPlayerView.pause()
    }

    override fun onStop() {
        super.onStop()
        videoPlayerView.stopPlayback()
    }
}
