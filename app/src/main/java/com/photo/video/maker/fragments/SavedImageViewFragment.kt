package com.photo.video.maker.fragments;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.FileProvider
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.facebook.ads.AdSize
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.gms.ads.AdView
import com.photo.video.maker.BuildConfig
import com.photo.video.maker.R
import com.photo.video.maker.dialogs.DeleteThisDialog
import com.photo.video.maker.interfaces.DialogDelToFragmentInteraface
import com.photo.video.maker.utils.BillingHelper
import com.photo.video.maker.utils.DeleteUtils
import com.photo.video.maker.utils.MyConstants
import com.photo.video.maker.utils.MyEssentials
import java.io.File

class SavedImageViewFragment : Fragment(), DialogDelToFragmentInteraface, BottomNavigationView.OnNavigationItemSelectedListener {
    private var imageName: String? = null
    private var imagePath: String? = null
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var billingHelper: BillingHelper
    private lateinit var mAdView: AdView
    private lateinit var fbAdView: com.facebook.ads.AdView
    private lateinit var fbAdContainer: LinearLayout
    private lateinit var mContext: Context
    private lateinit var imageView: PhotoView
    private lateinit var bottomNavView: BottomNavigationView

    companion object {
        fun getInstance(path: String?, name: String?): SavedImageViewFragment {
            val args = Bundle()
            args.putString(MyConstants.SAVED_IMAGE_VIEW_PATH_KEY, path)
            args.putString(MyConstants.SAVED_IMAGE_VIEW_NAME_KEY, name)
            val fragment = SavedImageViewFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val viewNew = inflater.inflate(R.layout.fragment_saved_image_view, container, false)
        initViews(viewNew)
        activity?.title = imageName
        return viewNew
    }

    private fun initViews(view: View) {
        bottomNavView = view.findViewById(R.id.bottomNavMenu)
        bottomNavView.setOnNavigationItemSelectedListener(this)
        imageView = view.findViewById(R.id.zoomableView)
        imageName = arguments?.getString(MyConstants.SAVED_IMAGE_VIEW_NAME_KEY)
        imagePath = arguments?.getString(MyConstants.SAVED_IMAGE_VIEW_PATH_KEY)
        mFragmentManager = activity!!.supportFragmentManager
        billingHelper = BillingHelper(mContext)
        mAdView = view.findViewById(R.id.adView)
        fbAdView = com.facebook.ads.AdView(mContext, resources.getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50)
        fbAdContainer = view.findViewById(R.id.fbAdContainer)
        Glide.with(mContext).load(imagePath).into(imageView)
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = activity!!
    }

    override fun onResume() {
        super.onResume()
        if (activity != null) {
            mContext = activity!!
        }
    }

    override fun deleteThisImageOrVideo(flag: Boolean) {
        val file =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Images" + File.separator)
        if (flag) {
            DeleteUtils.deleteSingleFile(file, imageName)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shareBtnMenu -> {
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    val uriShare = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID, File(imagePath))
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uriShare)
                    shareIntent.type = "image/*"
                    startActivity(Intent.createChooser(shareIntent, "Share Using"))
                } catch (e: Exception) {
                }
            }
            R.id.deleteBtnMenu -> {
                val deleteDialog =
                        DeleteThisDialog(mContext, activity!!, "This Image will be deleted fom creations folder?", this)
                deleteDialog.show()
            }
        }
        return true
    }
}
