package com.photo.video.maker.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.photo.video.maker.R;
import com.photo.video.maker.adapters.FiltersAdapter;
import com.photo.video.maker.adapters.ToolsAdapter;
import com.photo.video.maker.dialogs.AddStickerDialog;
import com.photo.video.maker.dialogs.AddTextDialog;
import com.photo.video.maker.interfaces.DialogStickerToEditorFragment;
import com.photo.video.maker.interfaces.DialogTextToEditorFragment;
import com.photo.video.maker.interfaces.OnToolsItemSelectedInterface;
import com.photo.video.maker.model.FilterModel;
import com.photo.video.maker.utils.*;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;

import static android.app.Activity.RESULT_OK;

public class EditorFragment extends Fragment implements OnToolsItemSelectedInterface, View.OnClickListener, DialogTextToEditorFragment, DialogStickerToEditorFragment {

    ArrayList<String> editedList = new ArrayList<>();
    public static int positionReceived;
    private PhotoEditor photoEditor;
    RecyclerView toolsRecyclerView;
    ToolsAdapter toolsAdapter;
    RecyclerView filterRecyclerView;
    FiltersAdapter filtersAdapter;
    List<FilterModel> filterModelList = new ArrayList<>();
    boolean isFilterViewVisible;
    ImageView moveBack;
    File files;
    Context mContext;
    LinearLayout fbAdContainer;
    InterstitialAd mInterstitialAd;
    com.facebook.ads.InterstitialAd fbInterstitialAd;
    BillingHelper billingHelper;
    public static File imagePath;

    public static EditorFragment getInstance(ArrayList<String> list, String data, int position) {
        Bundle args = new Bundle();
        args.putString(MyConstants.EDITOR_IMAGE_KEY, data);
        args.putStringArrayList(MyConstants.EDITOR_LIST_KEY, list);
        args.putInt(MyConstants.EDITOR_POSITION_KEY, position);
        EditorFragment fragment = new EditorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_editor, container, false);
        initViews(view);
        toolsAdapter = new ToolsAdapter(getActivity(), this);
        toolsRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        toolsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        toolsRecyclerView.setAdapter(toolsAdapter);
        filterModelList = ImageUtils.updateList(filterModelList);
        filtersAdapter = new FiltersAdapter(getActivity(), filterModelList);
        filterRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        filterRecyclerView.setItemAnimator(new DefaultItemAnimator());
        filterRecyclerView.setAdapter(filtersAdapter);
        initFilterListener();
        String imagePath = null;
        try {
            imagePath = getArguments().getString(MyConstants.EDITOR_IMAGE_KEY);
            editedList = getArguments().getStringArrayList(MyConstants.EDITOR_LIST_KEY);
            positionReceived = getArguments().getInt(MyConstants.EDITOR_POSITION_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PhotoEditorView imageView = view.findViewById(R.id.imageView);
        Glide.with(mContext).load(imagePath).into(imageView.getSource());
        photoEditor = new PhotoEditor.Builder(mContext, imageView)
                .setPinchTextScalable(true)
                .build();
        billingHelper = new BillingHelper(getActivity());
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
            mInterstitialAd = MyEssentials.loadAdMobInterstitialAd(mContext);
            fbInterstitialAd = MyEssentials.loadFbInterstitialAd(mContext);
        }
        return view;
    }

    private void initViews(View view) {
        toolsRecyclerView = view.findViewById(R.id.toolsRecyclerViewInner);
        filterRecyclerView = view.findViewById(R.id.filterRecyclerView);
        moveBack = view.findViewById(R.id.moveBack);
        view.findViewById(R.id.undoBtn).setOnClickListener(this);
        view.findViewById(R.id.redoBtn).setOnClickListener(this);
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        moveBack.setOnClickListener(this);
    }

    private void initFilterListener() {
        filterRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext, filterRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ImageUtils.filterSelectClick(photoEditor, position);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    @Override
    public void onToolSelected(ToolType toolType) {
        switch (toolType) {
            case TEXT:
                AddTextDialog dialog = new AddTextDialog(mContext, this);
                dialog.show();
                break;
            case STICKERS:
                AddStickerDialog dialog1 = new AddStickerDialog(mContext, this);
                dialog1.show();
                break;
            case EFFECTS:
                isFilterViewVisible = true;
                toolsRecyclerView.setVisibility(View.INVISIBLE);
                filterRecyclerView.setVisibility(View.VISIBLE);
                moveBack.setVisibility(View.VISIBLE);
                Toast.makeText(mContext, "Press the cross button to close frame view", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.doneBtn:
                if (MyEssentials.isMediation) {
                    if (fbInterstitialAd.isAdLoaded() && !fbInterstitialAd.isAdInvalidated()) {
                        fbInterstitialAd.show();
                        fbInterstitialAd.setAdListener(new FbAdListener() {
                            @Override
                            public void onInterstitialDismissed(Ad ad) {
                                super.onInterstitialDismissed(ad);
                                MyEssentials.isMediation = false;
                                MyEssentials.setTimer(5000);
                                saveAndMoveForward();
                            }

                            @Override
                            public void onAdClicked(Ad ad) {
                                super.onAdClicked(ad);
                                MyEssentials.isMediation = false;
                                MyEssentials.setTimer(1500);
                                MyEssentials.logEvents(AnalyticsConstants.IMAGE_EDITING_ACTIVITY_DONE_BUTTON_CLICKED, getActivity());
                            }
                        });
                    } else if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                        mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                MyEssentials.isMediation = false;
                                MyEssentials.setTimer(5000);
                                saveAndMoveForward();
                            }

                            @Override
                            public void onAdClicked() {
                                super.onAdClicked();
                                MyEssentials.isMediation = false;
                                MyEssentials.setTimer(15000);
                                MyEssentials.logEvents(AnalyticsConstants.IMAGE_EDITING_ACTIVITY_DONE_BUTTON_CLICKED, getActivity());
                            }
                        });
                    } else {
                        saveAndMoveForward();
                    }
                } else {
                    saveAndMoveForward();
                }
                break;
            case R.id.undoBtn:
                photoEditor.undo();
                break;
            case R.id.redoBtn:
                photoEditor.redo();
                break;
            case R.id.moveBack:
                filterRecyclerView.setVisibility(View.GONE);
                toolsRecyclerView.setVisibility(View.VISIBLE);
                isFilterViewVisible = false;
                moveBack.setVisibility(View.GONE);
                break;
        }
    }

    private void saveAndMoveForward() {
        if (editedList != null && editedList.size() > 0) {
            imagePath = SaveUtils.saveEditedImage(files, getActivity(), photoEditor);
            if (imagePath != null) {
                try {
                    editedList.set(positionReceived, imagePath.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final SelectedImagesFragment fragment = SelectedImagesFragment.getInstance(editedList, false);
                if (editedList != null && editedList.size() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragment).commitAllowingStateLoss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 3000);
                }
            } else {
                Toast.makeText(mContext, "Cannot edit this image!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == MyConstants.STICKER_GALLERY_PICKER_INTENT) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                assert selectedImage != null;
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    cursor.moveToFirst();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int columnIndex = 0;
                try {
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String imgDecodableString = null;
                try {
                    imgDecodableString = cursor.getString(columnIndex);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
                if (bitmap != null && photoEditor != null) {
                    photoEditor.addImage(bitmap);
                }
            }
        }
    }

    @Override
    public void setPhotoEditorText(String text, int color) {
        photoEditor.addText(text, color);
    }

    @Override
    public void setPhotoEditorSticker(int position) {
        ImageUtils.setSticker(photoEditor, position, getActivity());
    }

    @SuppressLint("IntentReset")
    @Override
    public void setIntentCall() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, MyConstants.STICKER_GALLERY_PICKER_INTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
