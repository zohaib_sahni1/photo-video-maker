package com.photo.video.maker.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.DialogDelToFragmentInteraface;


public class DeleteThisDialog extends Dialog implements View.OnClickListener {
    private String headerText;
    private DialogDelToFragmentInteraface callBack;
    private Activity activity;
    private Context context;

    public DeleteThisDialog(@NonNull Context context, Activity activity, String headerText, DialogDelToFragmentInteraface callBack) {
        super(context);
        this.headerText = headerText;
        this.callBack = callBack;
        this.activity = activity;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_delete_layout);
        findViewById(R.id.deleteBtnDialog).setOnClickListener(this);
        findViewById(R.id.cancelBtnDialog).setOnClickListener(this);
        TextView headerView = findViewById(R.id.headerDialog);
        headerView.setText(headerText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.deleteBtnDialog:
                callBack.deleteThisImageOrVideo(true);
                dismissIt();
                activity.onBackPressed();
                break;
            case R.id.cancelBtnDialog:
                dismissIt();
                break;

        }
    }

    private void dismissIt() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
