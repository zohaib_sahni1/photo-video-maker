package com.photo.video.maker.model;

public class StickerModel {
    private int stickerImage;

    public StickerModel(int stickerImage) {
        this.stickerImage = stickerImage;
    }

    public int getImageView() {
        return stickerImage;
    }

    public void setImageView(int stickerImage) {
        this.stickerImage = stickerImage;
    }
}
