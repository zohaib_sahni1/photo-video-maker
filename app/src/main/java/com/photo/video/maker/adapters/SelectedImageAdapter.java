package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.photo.video.maker.R;
import com.photo.video.maker.fragments.EditorFragment;
import com.photo.video.maker.interfaces.GetImageViewInterface;

import java.util.ArrayList;

public class SelectedImageAdapter extends RecyclerView.Adapter<SelectedImageAdapter.MyViewHolder> {
    private Context context;
    private android.support.v4.app.FragmentManager fragmentManager;
    private ArrayList<String> newPathStrings = new ArrayList<>();

    public SelectedImageAdapter(Context context, android.support.v4.app.FragmentManager fragmentManager, ArrayList<String> newPathStrings) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.newPathStrings = newPathStrings;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selected_image_item_new, parent, false);
        return new MyViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        //Glide.with(context).load(newPathStrings.get(holder.getAdapterPosition())).into(holder.imageView);
        if (position != RecyclerView.NO_POSITION) {
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions().override(720, 720).centerCrop()).load(newPathStrings.get(holder.getAdapterPosition())).into(holder.imageView);
            holder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EditorFragment fragment = EditorFragment.getInstance(newPathStrings, newPathStrings.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
                }
            });
            holder.crossBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        newPathStrings.remove(position);
                        notifyItemRemoved(position);
                        //notifyItemRangeChanged(position, newPathStrings.size());
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return newPathStrings.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements GetImageViewInterface {
        private ImageView imageView;
        private ImageView crossBtn;
        private ImageView editBtn;

        MyViewHolder(View itemView, Context context) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            crossBtn = itemView.findViewById(R.id.crossBtn);
            editBtn = itemView.findViewById(R.id.editBtn);
        }

        @Override
        public ImageView getImageViewAdapter() {
            return imageView;
        }
    }

}
