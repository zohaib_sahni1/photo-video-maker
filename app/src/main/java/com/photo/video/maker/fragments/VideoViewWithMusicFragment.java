package com.photo.video.maker.fragments;


import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.MyConstants;
import com.photo.video.maker.utils.MyEssentials;


public class VideoViewWithMusicFragment extends Fragment implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    VideoView videoView;
    String videoPath;
    String action;
    Context mContext;
    MediaController mediaController;
    FragmentManager fragmentManager;
    LinearLayout fbAdContainer;
    BillingHelper billingHelper;

    public static VideoViewWithMusicFragment getInstance(String path, String action) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_VIEW_MUSIC_KEY, path);
        args.putString(MyConstants.VIDEO_VIEW_MUSIC_ACTION, action);
        VideoViewWithMusicFragment fragment = new VideoViewWithMusicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_view_with_music, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        fragmentManager = getActivity().getSupportFragmentManager();
        videoView = view.findViewById(R.id.videoView);
        mediaController = new MediaController(mContext);
        mediaController.setAnchorView(videoView);
        mediaController.setMediaPlayer(videoView);
        try {
            assert getArguments() != null;
            videoPath = getArguments().getString(MyConstants.VIDEO_VIEW_MUSIC_KEY);
            action = getArguments().getString(MyConstants.VIDEO_VIEW_MUSIC_ACTION);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        assert action != null;
        if (action.equalsIgnoreCase(MyConstants.TO_MUSIC_ACTION_NOTIFICATION)) {
            view.findViewById(R.id.backBtn).setVisibility(View.INVISIBLE);
        }
        videoView.setVideoURI(Uri.parse(videoPath));
        videoView.setMediaController(mediaController);
        videoView.requestFocus();
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.start();
        billingHelper = new BillingHelper(mContext);
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        videoView.start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                try {
                    fragmentManager.popBackStack();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.doneBtn:
                if (action.equalsIgnoreCase(MyConstants.TO_MUSIC_ACTION_SIMPLE)) {
                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                } else if (action.equalsIgnoreCase(MyConstants.TO_MUSIC_ACTION_NOTIFICATION)) {
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, new MainViewFragment()).commitAllowingStateLoss();
                }
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        try {
            if (mContext != null && !getActivity().isFinishing()) {
                mediaController.show(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
