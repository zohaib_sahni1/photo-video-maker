package com.photo.video.maker.dialogs;

;
import android.app.Dialog;
import android.content.Context;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.photo.video.maker.R;
import com.photo.video.maker.adapters.StickersAdapter;
import com.photo.video.maker.interfaces.DialogStickerToEditorFragment;
import com.photo.video.maker.utils.RecyclerTouchListener;

public class AddStickerDialog extends Dialog implements View.OnClickListener {
    private Context context;
    private DialogStickerToEditorFragment callback;

    public AddStickerDialog(@NonNull Context context, DialogStickerToEditorFragment callback) {
        super(context);
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_stickers_layout);
        RecyclerView recyclerView = findViewById(R.id.stickerRecyclerView);
        TextView galleryBtn = findViewById(R.id.galleryBtn);
        galleryBtn.setOnClickListener(this);
        findViewById(R.id.backBtn).setOnClickListener(this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        StickersAdapter adapter = new StickersAdapter(context);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                callback.setPhotoEditorSticker(position);
                dismiss();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismiss();
                break;
            case R.id.galleryBtn:
                callback.setIntentCall();
                dismiss();
                break;
        }
    }
}
