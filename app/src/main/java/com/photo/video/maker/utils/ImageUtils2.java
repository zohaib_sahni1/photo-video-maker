package com.photo.video.maker.utils;

import android.app.Activity;

import com.photo.video.maker.R;
import com.photo.video.maker.model.FrameModel;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ImageUtils2 {
    public static List<FrameModel> updateList(List<FrameModel> list) {
        list.add(new FrameModel(R.color.AliceBlue, "Alice Blue", "AliceBlue"));
        list.add(new FrameModel(R.color.AntiqueWhite, "Antique White", "AntiqueWhite"));
        list.add(new FrameModel(R.color.Aqua, "Aqua", "Aqua"));
        list.add(new FrameModel(R.color.Azure, "Azure", "Azure"));
        list.add(new FrameModel(R.color.Black, "Black", "Black"));
        list.add(new FrameModel(R.color.BlanchedAlmond, "Blanched Almond", "BlanchedAlmond"));
        list.add(new FrameModel(R.color.Blue, "Blue", "Blue"));
        list.add(new FrameModel(R.color.BlueViolet, "Blue Violet", "BlueViolet"));
        list.add(new FrameModel(R.color.Brown, "Brown", "Brown"));
        list.add(new FrameModel(R.color.Chartreuse, "Chartreuse", "Chartreuse"));
        list.add(new FrameModel(R.color.Chocolate, "Chocolate", "Chocolate"));
        list.add(new FrameModel(R.color.Cyan, "Cyan", "Cyan"));
        list.add(new FrameModel(R.color.DarkBlue, "Dark Blue", "DarkBlue"));
        list.add(new FrameModel(R.color.DarkCyan, "Dark Cyan", "DarkCyan"));
        list.add(new FrameModel(R.color.DarkGoldenRod, "Dark Golden Rod", "DarkGoldenRod"));
        list.add(new FrameModel(R.color.DarkGray, "Dark Gray", "DarkGray"));
        list.add(new FrameModel(R.color.DarkGreen, "Dark Green", "DarkGreen"));
        list.add(new FrameModel(R.color.Darkorange, "Dark orange", "DarkOrange"));
        list.add(new FrameModel(R.color.DarkRed, "Dark Red", "DarkRed"));
        list.add(new FrameModel(R.color.DarkSalmon, "Dark Salmon", "DarkSalmon"));
        list.add(new FrameModel(R.color.DarkSeaGreen, "Dark Sea Green", "DarkSeaGreen"));
        list.add(new FrameModel(R.color.DarkViolet, "Dark Violet", "DarkViolet"));
        list.add(new FrameModel(R.color.DeepPink, "Deep Pink", "DeepPink"));
        list.add(new FrameModel(R.color.DeepSkyBlue, "Deep Sky Blue", "DeepSkyBlue"));
        list.add(new FrameModel(R.color.GhostWhite, "Ghost White", "GhostWhite"));
        list.add(new FrameModel(R.color.Gold, "Gold", "Gold"));
        list.add(new FrameModel(R.color.GoldenRod, "Golden Rod", "GoldenRod"));
        list.add(new FrameModel(R.color.Gray, "Gray", "Gray"));
        list.add(new FrameModel(R.color.Green, "Green", "Green"));
        list.add(new FrameModel(R.color.HoneyDew, "Honey Dew", "HoneyDew"));
        list.add(new FrameModel(R.color.Indigo, "Indigo", "Indigo"));
        list.add(new FrameModel(R.color.LawnGreen, "Lawn Green", "LawnGreen"));
        list.add(new FrameModel(R.color.LemonChiffon, "Lemon Chiffon", "LemonChiffon"));
        list.add(new FrameModel(R.color.LightBlue, "Light Blue", "LightBlue"));
        list.add(new FrameModel(R.color.LightGreen, "Light Green", "LightGreen"));
        list.add(new FrameModel(R.color.LightGrey, "Light Grey", "LightGray"));
        list.add(new FrameModel(R.color.LightPink, "Light Pink", "LightPink"));
        list.add(new FrameModel(R.color.LightSteelBlue, "Light Steel Blue", "LightSteelBlue"));
        list.add(new FrameModel(R.color.Maroon, "Maroon", "Maroon"));
        list.add(new FrameModel(R.color.Navy, "Navy", "Navy"));
        list.add(new FrameModel(R.color.Orange, "Orange", "Orange"));
        list.add(new FrameModel(R.color.OrangeRed, "Orange Red", "OrangeRed"));
        list.add(new FrameModel(R.color.Peru, "Peru", "Peru"));
        list.add(new FrameModel(R.color.Pink, "Pink", "Pink"));
        list.add(new FrameModel(R.color.Purple, "Purple", "Purple"));
        list.add(new FrameModel(R.color.Red, "Red", "Red"));
        list.add(new FrameModel(R.color.SkyBlue, "Sky Blue", "SkyBlue"));
        list.add(new FrameModel(R.color.Snow, "Snow", "Snow"));
        list.add(new FrameModel(R.color.White, "White", "White"));
        list.add(new FrameModel(R.color.WhiteSmoke, "White Smoke", "WhiteSmoke"));
        list.add(new FrameModel(R.color.Yellow, "Yellow", "Yellow"));
        return list;
    }

    public static void saveFont(Activity activity) {
        String tutorialKey = "SOME_KEY";
        Boolean firstTime = activity.getPreferences(MODE_PRIVATE).getBoolean(tutorialKey, true);
        if (firstTime) {
            SaveFrames.saveFont(R.font.aller_rg, activity);
            activity.getPreferences(MODE_PRIVATE).edit().putBoolean(tutorialKey, false).apply();
        }
    }

}
