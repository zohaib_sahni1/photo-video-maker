package com.photo.video.maker.interfaces;

import java.util.ArrayList;

public interface SelectedFragmentToAdapterInterface {
    ArrayList<String> getOriginalList();
}
