package com.photo.video.maker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.DialogDurationToSelectedFragment;
import com.photo.video.maker.utils.AnalyticsConstants;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.FbAdListener;
import com.photo.video.maker.utils.MyEssentials;

import java.math.BigInteger;

public class AddDurationDialog extends Dialog implements View.OnClickListener {
    private EditText editText;
    private DialogDurationToSelectedFragment callBack;
    private Context context;
    private InterstitialAd mInterstitialAd;
    private com.facebook.ads.InterstitialAd fbInterstitialAd;

    public AddDurationDialog(@NonNull Context context, DialogDurationToSelectedFragment callBack) {
        super(context);
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_video_duration_layout);
        TextView addDurationBtn = findViewById(R.id.addDurationBtn);
        editText = findViewById(R.id.editText);
        ImageView backBtn = findViewById(R.id.backBtn);
        addDurationBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
        BillingHelper billingHelper = new BillingHelper(context);
        if (billingHelper.shouldShowAds()) {
            mInterstitialAd = MyEssentials.loadAdMobInterstitialAd(context);
            fbInterstitialAd = MyEssentials.loadFbInterstitialAd(context);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismissIt();
                break;
            case R.id.addDurationBtn:
                if (editText.getText().toString().equals("")) {
                    Toast.makeText(context, "Please enter duration in seconds!", Toast.LENGTH_SHORT).show();
                } else {
                    if (MyEssentials.isMediation) {
                        if (fbInterstitialAd.isAdLoaded() && !fbInterstitialAd.isAdInvalidated()) {
                            fbInterstitialAd.show();
                            fbInterstitialAd.setAdListener(new FbAdListener() {
                                @Override
                                public void onInterstitialDismissed(Ad ad) {
                                    super.onInterstitialDismissed(ad);
                                    BigInteger duration = new BigInteger(editText.getText().toString());
                                    callBack.setDuration(duration.intValue());
                                    MyEssentials.isMediation = false;
                                    MyEssentials.setTimer(5000);
                                }

                                @Override
                                public void onAdClicked(Ad ad) {
                                    super.onAdClicked(ad);
                                    MyEssentials.logEvents(AnalyticsConstants.DURATION_DIALOG_TO_VIDEO_VIEW_ACTIVITY, context);
                                    MyEssentials.isMediation = false;
                                    MyEssentials.setTimer(15000);
                                }
                            });
                        } else if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                            mInterstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    BigInteger duration = new BigInteger(editText.getText().toString());
                                    callBack.setDuration(duration.intValue());
                                    MyEssentials.isMediation = false;
                                    MyEssentials.setTimer(5000);
                                }

                                @Override
                                public void onAdClicked() {
                                    super.onAdClicked();
                                    MyEssentials.isMediation = false;
                                    MyEssentials.setTimer(15000);
                                    MyEssentials.logEvents(AnalyticsConstants.DURATION_DIALOG_TO_VIDEO_VIEW_ACTIVITY, context);
                                }
                            });
                        } else {
                            BigInteger duration = new BigInteger(editText.getText().toString());
                            callBack.setDuration(duration.intValue());
                        }
                    } else {
                        BigInteger duration = new BigInteger(editText.getText().toString());
                        callBack.setDuration(duration.intValue());
                    }
                }
                dismissIt();
                break;
        }
    }

    private void dismissIt() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
