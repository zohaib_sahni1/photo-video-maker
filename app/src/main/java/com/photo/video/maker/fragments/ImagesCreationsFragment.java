package com.photo.video.maker.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.ImagesCreationsAdapter;
import com.photo.video.maker.dialogs.DeleteThisDialog;
import com.photo.video.maker.interfaces.DialogDelToFragmentInteraface;
import com.photo.video.maker.utils.*;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

public class ImagesCreationsFragment extends Fragment implements View.OnClickListener, DialogDelToFragmentInteraface {
    FragmentManager fragmentManager;
    private RecyclerView imagesRecyclerView;
    ImagesCreationsAdapter adapter;
    TextView emptyText;
    FloatingActionButton deleteBtn;
    private String[] pathStrings;
    private String[] nameStrings;
    private File[] listFile;
    int listFileLenght;
    File file;
    boolean success;
    Context mContext;
    LinearLayout fbAdContainer;
    BillingHelper billingHelper;
    private TextView countText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_images_creations, container, false);
        initViews(view);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bindData();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            mContext = getActivity();
        }
    }

    private void initViews(View view) {
        fragmentManager = getActivity().getSupportFragmentManager();
        emptyText = view.findViewById(R.id.emptyText);
        deleteBtn = view.findViewById(R.id.delFloatingBtn);
        countText = view.findViewById(R.id.countText);
        deleteBtn.setOnClickListener(this);
        emptyText.setOnClickListener(this);
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        imagesRecyclerView = view.findViewById(R.id.recyclerView);
        billingHelper = new BillingHelper(getActivity());
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
        }
    }

    private void bindData() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Photo Video Editor" + File.separator + "Video Editor Images" + File.separator);
            success = file.mkdir();
        }
        if (file.exists()) {

            if (file.isDirectory()) {
                try {
                    listFile = file.listFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (listFile != null) {
                    try {
                        listFileLenght = listFile.length;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pathStrings = new String[listFileLenght];
                    nameStrings = new String[listFileLenght];

                    for (int i = 0; i < listFileLenght; i++) {
                        pathStrings[i] = listFile[i].getAbsolutePath();
                        nameStrings[i] = listFile[i].getName();
                    }
                }
            }
            if (nameStrings.length == 0) {
                deleteBtn.hide();
                emptyText.setVisibility(View.VISIBLE);
            } else {
                deleteBtn.show();
                emptyText.setVisibility(View.GONE);
            }
        } else {
            Toast.makeText(getActivity(), "Empty", Toast.LENGTH_SHORT).show();
            pathStrings = null;
            nameStrings = null;
            try {
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        //imagesRecyclerView.setLayoutManager(mLayoutManager);
        imagesRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        try {
            Collections.reverse(Arrays.asList(pathStrings));
            Collections.reverse(Arrays.asList(nameStrings));
        } catch (Exception e) {
            e.printStackTrace();
        }
        countText.setText(String.valueOf(pathStrings.length));
        adapter = new ImagesCreationsAdapter(getActivity(), pathStrings);
        imagesRecyclerView.setAdapter(adapter);
        imagesRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), imagesRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (position % 5 == 0) {
                    return;
                }
                int newPos = (position - Math.round((position / 5) + 1));
                SavedImageViewFragment fragment = SavedImageViewFragment.Companion.getInstance(pathStrings[newPos], nameStrings[newPos]);
                fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                fragmentManager.popBackStack();
                break;
            case R.id.delFloatingBtn:
                DeleteThisDialog dialog = new DeleteThisDialog(mContext, getActivity(), "All the Images will be deleted fom creations folder?", this);
                dialog.show();
                countText.setText(String.valueOf(pathStrings.length));
                break;
        }
    }

    @Override
    public void deleteThisImageOrVideo(boolean flag) {
        File file = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Images" + File.separator);
        if (flag) {
            DeleteUtils.deleteAllFiles(file, nameStrings, listFileLenght);
            try {
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
