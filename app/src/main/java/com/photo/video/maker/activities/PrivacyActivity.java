package com.photo.video.maker.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.MyConstants;

public class PrivacyActivity extends AppCompatActivity implements View.OnClickListener, CheckBox.OnCheckedChangeListener {
    private WebView webView;
    private CheckBox checkBox;
    private Button doneBtn;
    private boolean isFirestTime;
    private boolean myIsChecked;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        webView = findViewById(R.id.webView);
       /* webView.loadUrl("file:///android_asset/privacy_file.html");*/
        webView.loadUrl("https://www.tripadvisor.com/Tourism-g294260-Boracay_Malay_Aklan_Province_Panay_Island_Visayas-Vacations.html");
        checkBox = findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(this);
        doneBtn = findViewById(R.id.doneBtn);
        doneBtn.setOnClickListener(this);
        sharedPreferences = getSharedPreferences("Preference", MODE_PRIVATE);
        isFirestTime = sharedPreferences.getBoolean("FirstRun", true);
        if (!isFirestTime) {
            Intent intent = new Intent(PrivacyActivity.this, MainActivity.class);
            intent.putExtra(MyConstants.FRAGMENT_SELECTION_KEY, "main_view_fragment");
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.doneBtn:
                isFirestTime = false;
                sharedPreferences.edit().putBoolean("FirstRun", false).apply();
                Intent intent = new Intent(PrivacyActivity.this, MainActivity.class);
                intent.putExtra(MyConstants.FRAGMENT_SELECTION_KEY, "main_view_fragment");
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            myIsChecked = true;
            doneBtn.setEnabled(true);
        } else {
            myIsChecked = false;
            doneBtn.setEnabled(false);
        }
    }
}
