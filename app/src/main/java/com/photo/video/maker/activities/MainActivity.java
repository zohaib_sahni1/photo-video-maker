package com.photo.video.maker.activities;;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import com.photo.video.maker.R;
import com.photo.video.maker.fragments.MainViewFragment;
import com.photo.video.maker.fragments.SelectedImagesFragment;
import com.photo.video.maker.fragments.VideoViewFragment;
import com.photo.video.maker.fragments.VideoViewWithMusicFragment;
import com.photo.video.maker.utils.MyConstants;


import java.util.List;

public class MainActivity extends AppCompatActivity {
    MainViewFragment mainViewFragment;
    VideoViewFragment videoViewFragment;
    android.support.v4.app.FragmentManager fragmentManager;
    String titleReceived;
    String pathReceived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        }
        setContentView(R.layout.activity_main);
        Intent i = getIntent();
        titleReceived = i.getStringExtra(MyConstants.FRAGMENT_SELECTION_KEY);
        pathReceived = i.getStringExtra(MyConstants.FRAGMENT_SELECTION_PATH);
        mainViewFragment = new MainViewFragment();
        videoViewFragment = new VideoViewFragment();
        fragmentManager = getSupportFragmentManager();
        if (titleReceived.equalsIgnoreCase("video_view_with_music_fragment")) {
            VideoViewWithMusicFragment videoViewWithMusic = VideoViewWithMusicFragment.getInstance(pathReceived, MyConstants.TO_MUSIC_ACTION_NOTIFICATION);
            fragmentManager.beginTransaction().add(R.id.frameLayout, videoViewWithMusic).commit();
        } else if (titleReceived.equalsIgnoreCase("main_view_fragment")) {
            fragmentManager.beginTransaction().add(R.id.frameLayout, mainViewFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        backPressed();
    }

    public void backPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (getVisibleFragment() instanceof SelectedImagesFragment) {
                getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            Intent intent = new Intent(this, ExitActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }

    //new added
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
