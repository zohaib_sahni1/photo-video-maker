package com.photo.video.maker.fcmnotifications;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import com.photo.video.maker.R;


import static android.content.Context.NOTIFICATION_SERVICE;

public class FirebaseNotificationProcedure {
    private Context mCtx;
    @SuppressLint("StaticFieldLeak")
    private static FirebaseNotificationProcedure mInstance;

    private FirebaseNotificationProcedure(Context context) {
        mCtx = context;
    }

    public static synchronized FirebaseNotificationProcedure getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new FirebaseNotificationProcedure(context);
        }
        return mInstance;
    }

    void displayFirebasePushNotification(String title, final String body, String description, Bitmap bitmap, Bitmap bitIcon, Context context) {

        RemoteViews notificationViewBig = new RemoteViews(context.getPackageName(), R.layout.fcm_notification_big);
        RemoteViews notificationViewSmall = new RemoteViews(context.getPackageName(), R.layout.fcm_notification_small);
        notificationViewBig.setTextViewText(R.id.text, description);
        notificationViewSmall.setTextViewText(R.id.text, description);
        notificationViewBig.setTextViewText(R.id.title, title);
        notificationViewSmall.setTextViewText(R.id.title, title);
        notificationViewSmall.setImageViewBitmap(R.id.image_app, bitIcon);
        notificationViewBig.setImageViewBitmap(R.id.image_pic, bitmap);

        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mCtx, FcmConstants.CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_bell_icon)
                        .setCustomContentView(notificationViewSmall)
                        .setCustomBigContentView(notificationViewBig);
        Intent resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(body));
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            mBuilder.setContentIntent(pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NotificationManager mNotifyMgr =
                (NotificationManager) mCtx.getSystemService(NOTIFICATION_SERVICE);

        if (mNotifyMgr != null) {
            try {
                mNotifyMgr.notify(1, mBuilder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
