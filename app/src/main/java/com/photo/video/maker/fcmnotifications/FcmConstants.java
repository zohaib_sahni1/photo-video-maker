package com.photo.video.maker.fcmnotifications;

class FcmConstants {
    static final String CHANNEL_ID = "my_channel";
    static final String CHANNEL_NAME = "Channel new";
    static final String CHANNEL_DESCRIPTION = "Notification channel";
}
