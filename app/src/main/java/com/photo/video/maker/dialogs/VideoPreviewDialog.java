package com.photo.video.maker.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.photo.video.maker.R;
import com.photo.video.maker.adapters.PreviewAdapter;
import com.photo.video.maker.interfaces.VideoPrviewToFragment;
import com.photo.video.maker.utils.MySlideApplication;
import com.photo.video.maker.utils.MySlideShow;

import java.util.ArrayList;

public class VideoPreviewDialog extends Dialog implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    Context context;
    private ArrayList<String> imagesList;
    private int position = -1;
    private ViewPager viewPager;
    private int duration;
    private Handler handler;
    private Runnable runnable;
    private VideoPrviewToFragment callBack;

    public VideoPreviewDialog(@NonNull Context context, ArrayList<String> imagesList, int duration, VideoPrviewToFragment callBack) {
        super(context);
        this.context = context;
        this.duration = duration;
        this.imagesList = imagesList;
        this.callBack = callBack;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_video_preview_layout);
        viewPager = findViewById(R.id.viewPager);
        BottomNavigationView bottomNavigationSecond = findViewById(R.id.effectsBottomMenu);
        bottomNavigationSecond.setOnNavigationItemSelectedListener(this);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        PreviewAdapter previewAdapter = new PreviewAdapter(context, imagesList);
        try {
            viewPager.setAdapter(previewAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.doneBtn).setOnClickListener(this);
        handler = new Handler();
    }

    private void executeCommand() {
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        runnable = new Runnable() {
            public void run() {
                if (position >= imagesList.size()) {
                    position = -1;
                } else {
                    position = position + 1;
                }
                try {
                    viewPager.setCurrentItem(position, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    handler.postDelayed(this, duration);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        runnable.run();
    }

    private void setAnimationToViewPager(final int animationFromAnimus) {
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                Animation animation = AnimationUtils.loadAnimation(context, animationFromAnimus);
                page.setAnimation(animation);
                animation.start();
                page.animate();

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismiss();
                break;
            case R.id.doneBtn:
                dismiss();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.fadeInEffect:
                setAnimationToViewPager(R.anim.fade_in);
                MySlideApplication.Companion.setSLIDE_EFFECT(MySlideShow.Companion.getFADE_IN());
                callBack.setEffect("Fade In");
                executeCommand();
                break;
            case R.id.rotateEffect:
                setAnimationToViewPager(R.anim.rotate_animation);
                MySlideApplication.Companion.setSLIDE_EFFECT(MySlideShow.Companion.getROTATE());
                callBack.setEffect("Rotation");
                executeCommand();
                break;
            case R.id.slideIn:
                setAnimationToViewPager(R.anim.slide_from_right);
                MySlideApplication.Companion.setSLIDE_EFFECT(MySlideShow.Companion.getSLIDE_IN());
                callBack.setEffect("Slide In");
                executeCommand();
                break;
            case R.id.fadeOut:
                setAnimationToViewPager(R.anim.fade_out);
                MySlideApplication.Companion.setSLIDE_EFFECT(MySlideShow.Companion.getFADE_OUT());
                callBack.setEffect("Fade Out");
                executeCommand();
                break;
            case R.id.noEffect:
                MySlideApplication.Companion.setSLIDE_EFFECT(MySlideShow.Companion.getNONE());
                callBack.setEffect(" ");
                executeCommand();
                break;
        }
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            viewPager.setAdapter(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
