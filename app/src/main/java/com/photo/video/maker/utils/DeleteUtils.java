package com.photo.video.maker.utils;

import java.io.File;
import java.util.ArrayList;

public class DeleteUtils {
    public static void deleteSingleFile(File fileMain, String fileName) {
        if (fileMain.exists()) {
            File oneFile = new File(fileMain.toString(), fileName);
            if (oneFile.exists()) {
                try {
                    boolean success = oneFile.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void deleteAllFiles(File fileMain, String[] fileNamesString, int listFileLength) {
        if (fileMain.exists()) {
            for (int i = 0; i < listFileLength; i++) {
                File singleFile = new File(fileMain.toString(), fileNamesString[i]);
                if (singleFile.exists()) {
                    try {
                        boolean success1 = singleFile.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void deleteTempFiles(File fileMain, ArrayList<String> fileNamesString, int listFileLength) {
        if (fileMain.exists()) {
            for (int i = 0; i < listFileLength; i++) {
                File singleFile = new File(fileMain.toString(), fileNamesString.get(i));
                if (singleFile.exists()) {
                    try {
                        boolean success1 = singleFile.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
