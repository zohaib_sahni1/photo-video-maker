package com.photo.video.maker.interfaces;

import android.widget.TextView;

public interface CounterTextInterface {
    void setCount(int val);
}
