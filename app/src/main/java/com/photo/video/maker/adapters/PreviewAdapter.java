package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;

import java.util.ArrayList;

public class PreviewAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> imagesList;
    private LayoutInflater layoutInflater;

    public PreviewAdapter(Context context, ArrayList<String> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagesList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.view_pager_item_layout, container, false);
        final ImageView imageView = itemView.findViewById(R.id.imageView);
        try {
            Glide.with(context).load(imagesList.get(position)).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            container.addView(itemView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        try {
            container.removeView((LinearLayout) object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            Toast.makeText(context, "Failed to show preview", Toast.LENGTH_SHORT).show();
        }
    }

}