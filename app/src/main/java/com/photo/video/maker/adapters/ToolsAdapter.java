package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.OnToolsItemSelectedInterface;
import com.photo.video.maker.model.ToolsModel;
import com.photo.video.maker.utils.ToolType;

import java.util.ArrayList;
import java.util.List;

public class ToolsAdapter extends RecyclerView.Adapter<ToolsAdapter.MyViewHolder> {
    private List<ToolsModel> toolsModelList = new ArrayList<>();
    Context context;
    private OnToolsItemSelectedInterface onItemSelected;

    public ToolsAdapter(Context context, OnToolsItemSelectedInterface onItemSelected) {
        this.context = context;
        this.onItemSelected = onItemSelected;
        toolsModelList.add(new ToolsModel(R.drawable.text_off, ToolType.TEXT));
        toolsModelList.add(new ToolsModel(R.drawable.magic_off, ToolType.EFFECTS));
        toolsModelList.add(new ToolsModel(R.drawable.sticker_off, ToolType.STICKERS));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tools_row_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            ToolsModel item = toolsModelList.get(position);
            holder.imageView.setImageResource(item.getIcon());
        }
    }
    @Override
    public int getItemCount() {
        return toolsModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageIcon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemSelected.onToolSelected(toolsModelList.get(getLayoutPosition()).getToolType());
                }
            });
        }
    }
}
