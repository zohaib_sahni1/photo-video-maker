package com.photo.video.maker.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.photo.video.maker.R;

public class BillingHelper implements BillingProcessor.IBillingHandler {

    private BillingProcessor billingProcessor;

    public BillingHelper(Context context) {
        billingProcessor = new BillingProcessor(context, context.getString(R.string.billing_key), this);
        billingProcessor.initialize();
    }

    public boolean shouldShowAds(){
        return !billingProcessor.isPurchased("remove_ads");
    }


    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
}
