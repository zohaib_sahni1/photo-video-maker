package com.photo.video.maker.model;


import com.photo.video.maker.utils.ToolType;

public class ToolsModel {
    private int icon;
    private ToolType toolType;

    public ToolsModel(int icon, ToolType toolType) {
        this.icon = icon;
        this.toolType = toolType;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public ToolType getToolType() {
        return toolType;
    }

}
