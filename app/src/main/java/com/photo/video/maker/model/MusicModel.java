package com.photo.video.maker.model;

public class MusicModel {
    private String name;
    private String title;
    private String path;

    public MusicModel(String name, String title, String path) {
        this.name = name;
        this.title = title;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
