package com.photo.video.maker.interfaces;

public interface DialogTextToEditorFragment {
    void setPhotoEditorText(String text, int color);
}
