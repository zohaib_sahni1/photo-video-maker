package com.photo.video.maker.utils;

public enum ToolType {
    TEXT,
    EFFECTS,
    STICKERS
}
