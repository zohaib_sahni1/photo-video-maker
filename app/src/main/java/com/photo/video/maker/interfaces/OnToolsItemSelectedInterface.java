package com.photo.video.maker.interfaces;


import com.photo.video.maker.utils.ToolType;

public interface OnToolsItemSelectedInterface {
    void onToolSelected(ToolType toolType);
}
