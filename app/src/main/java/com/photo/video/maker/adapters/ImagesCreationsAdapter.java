package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.AnimationsUtils;
import com.photo.video.maker.utils.BillingHelper;

public class ImagesCreationsAdapter extends RecyclerView.Adapter<ImagesCreationsAdapter.MyViewHolder> {
    Context context;
    private String[] imagesPath;
    private int AD_TYPE = 0;
    private int CONTENT_TYPE = 1;
    private BillingHelper billingHelper;

    public ImagesCreationsAdapter(Context context, String[] imagesPath) {
        this.context = context;
        this.imagesPath = imagesPath;
        billingHelper = new BillingHelper(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        if (viewType == CONTENT_TYPE) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_creation_item, parent, false);
        } else if (viewType == AD_TYPE) {
            itemView = new AdView(context,
                    context.getString(R.string.fb_banner), AdSize.RECTANGLE_HEIGHT_250);
            if (billingHelper.shouldShowAds()) ((AdView) itemView).loadAd();
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            if (position % 5 == 0) return;
            int newPos = (position - Math.round((position / 5) + 1));
            Glide.with(context).load(imagesPath[newPos]).into(holder.imageView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 5 == 0) return AD_TYPE;
        else
            return CONTENT_TYPE;
    }

    @Override
    public int getItemCount() {
        int count = imagesPath.length;
        return count + Math.round((count / 4) + 1);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageItem);
            AnimationsUtils.setScaleAnimation(itemView.getRootView());
        }
    }
}
