package com.photo.video.maker.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import android.widget.TextView
import com.photo.video.maker.R
import com.photo.video.maker.interfaces.ProcessingDialogToVideoViewInterface

class ProcessingDialogNew : Dialog {
    private var mContext: Context
    private lateinit var proggressBar: ProgressBar
    private lateinit var textView: TextView
    private var textProcessing: String? = null
    private lateinit var goToBgBtn: TextView
    private var callBack: ProcessingDialogToVideoViewInterface

    constructor(context: Context?, textProcessing: String, callBack: ProcessingDialogToVideoViewInterface) : super(context) {
        this.mContext = context!!
        this.textProcessing = textProcessing
        this.callBack = callBack
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window!!.setWindowAnimations(R.style.CustomDialogAnimation)
        setContentView(R.layout.processing_dailog_layout_new)
        setCancelable(false)
        proggressBar = findViewById(R.id.progressBar)
        proggressBar.getIndeterminateDrawable().setColorFilter(mContext.getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY)
        textView = findViewById(R.id.textView)
        textView.text = textProcessing
        goToBgBtn = findViewById(R.id.goToBgBtn)
        val animation = AnimationUtils.loadAnimation(mContext, R.anim.bounce_animation)
        textView.startAnimation(animation)
        goToBgBtn.setOnClickListener {
            callBack.doAction();
        }
    }

    fun setTextToDialog(text: String) {
        textView.text = text
    }

    fun dismissIt() {
        try {
            dismiss()
        } catch (e: Exception) {
        }
        if (proggressBar.getVisibility() == View.VISIBLE) {
            proggressBar.setVisibility(View.GONE)
        }
    }

    fun setProgress(progress: Int) {
        proggressBar.progress = progress
    }
}