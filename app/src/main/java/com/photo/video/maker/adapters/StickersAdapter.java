package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;
import com.photo.video.maker.model.StickerModel;
import com.photo.video.maker.utils.AnimationsUtils;

import java.util.ArrayList;
import java.util.List;

public class StickersAdapter extends RecyclerView.Adapter<StickersAdapter.MyViewHodler> {
    private List<StickerModel> stickerModelList = new ArrayList<>();
    Context context;

    public StickersAdapter(Context context) {
        this.context = context;
        updateList();
    }

    @NonNull
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sticker, parent, false);
        return new MyViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHodler holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            StickerModel model = stickerModelList.get(position);
            Glide.with(context).load(model.getImageView()).into(holder.imageView);
        }
    }
    @Override
    public int getItemCount() {
        return stickerModelList.size();
    }

    class MyViewHodler extends RecyclerView.ViewHolder {
        ImageView imageView;

        MyViewHodler(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageSticker);
            AnimationsUtils.setScaleAnimation(itemView);
        }
    }

    private void updateList() {
        stickerModelList.add(new StickerModel(R.drawable.sticker_1));
        stickerModelList.add(new StickerModel(R.drawable.sticker_2));
        stickerModelList.add(new StickerModel(R.drawable.sticker_3));
        stickerModelList.add(new StickerModel(R.drawable.sticker_4));
        stickerModelList.add(new StickerModel(R.drawable.sticker_5));
        stickerModelList.add(new StickerModel(R.drawable.sticker_6));
        stickerModelList.add(new StickerModel(R.drawable.sticker_7));
        stickerModelList.add(new StickerModel(R.drawable.sticker_8));
        stickerModelList.add(new StickerModel(R.drawable.sticker_9));
        stickerModelList.add(new StickerModel(R.drawable.sticker_10));
        stickerModelList.add(new StickerModel(R.drawable.sticker_11));
        stickerModelList.add(new StickerModel(R.drawable.sticker_12));
        stickerModelList.add(new StickerModel(R.drawable.sticker_13));
        stickerModelList.add(new StickerModel(R.drawable.sticker_14));
        stickerModelList.add(new StickerModel(R.drawable.sticker_15));
    }
}
