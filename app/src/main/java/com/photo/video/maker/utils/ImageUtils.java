package com.photo.video.maker.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.List;

import com.photo.video.maker.R;
import com.photo.video.maker.model.FilterModel;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoFilter;

public class ImageUtils {
    public static List<FilterModel> updateList(List<FilterModel> filterModelList) {
        filterModelList.add(new FilterModel(R.drawable.none, "None"));
        filterModelList.add(new FilterModel(R.drawable.auto_fix, "Auto fix"));
        filterModelList.add(new FilterModel(R.drawable.flip_horizontal, "Flip Horizontal"));
        filterModelList.add(new FilterModel(R.drawable.flip_vertical, "Flip Vertical"));
        filterModelList.add(new FilterModel(R.drawable.rotate, "Rotate"));
        filterModelList.add(new FilterModel(R.drawable.brightness, "Brightness"));
        filterModelList.add(new FilterModel(R.drawable.contrast, "Contrast"));
        filterModelList.add(new FilterModel(R.drawable.posterize, "Posterize"));
        filterModelList.add(new FilterModel(R.drawable.saturate, "Saturate"));
        filterModelList.add(new FilterModel(R.drawable.sepia, "Sepia"));
        filterModelList.add(new FilterModel(R.drawable.sharpen, "Sharpen"));
        filterModelList.add(new FilterModel(R.drawable.temperature, "Temperature"));
        filterModelList.add(new FilterModel(R.drawable.gray_scale, "Gray Scale"));
        filterModelList.add(new FilterModel(R.drawable.lomish, "Lomish"));
        filterModelList.add(new FilterModel(R.drawable.negative, "Negative"));
        filterModelList.add(new FilterModel(R.drawable.documentary, "Documentary"));
        filterModelList.add(new FilterModel(R.drawable.due_tone, "Due Tone"));
        filterModelList.add(new FilterModel(R.drawable.fill_light, "Fill Light"));
        filterModelList.add(new FilterModel(R.drawable.fish_eye, "Fish Eye"));
        filterModelList.add(new FilterModel(R.drawable.grain, "Grain"));
        filterModelList.add(new FilterModel(R.drawable.tint, "Tint"));
        filterModelList.add(new FilterModel(R.drawable.vignette, "Vignette"));
        filterModelList.add(new FilterModel(R.drawable.cross_process, "Cross Process"));
        filterModelList.add(new FilterModel(R.drawable.black_white, "Black White"));
        return filterModelList;
    }

    public static void filterSelectClick(PhotoEditor photoEditor, int position) {
        switch (position) {
            case 0:
                photoEditor.setFilterEffect(PhotoFilter.NONE);
                break;
            case 1:
                photoEditor.setFilterEffect(PhotoFilter.AUTO_FIX);
                break;
            case 2:
                photoEditor.setFilterEffect(PhotoFilter.FLIP_HORIZONTAL);
                break;
            case 3:
                photoEditor.setFilterEffect(PhotoFilter.FLIP_VERTICAL);
                break;
            case 4:
                photoEditor.setFilterEffect(PhotoFilter.ROTATE);
                break;
            case 5:
                photoEditor.setFilterEffect(PhotoFilter.BRIGHTNESS);
                break;
            case 6:
                photoEditor.setFilterEffect(PhotoFilter.CONTRAST);
                break;
            case 7:
                photoEditor.setFilterEffect(PhotoFilter.POSTERIZE);
                break;
            case 8:
                photoEditor.setFilterEffect(PhotoFilter.SATURATE);
                break;
            case 9:
                photoEditor.setFilterEffect(PhotoFilter.SEPIA);
                break;
            case 10:
                photoEditor.setFilterEffect(PhotoFilter.SHARPEN);
                break;
            case 11:
                photoEditor.setFilterEffect(PhotoFilter.TEMPERATURE);
                break;
            case 12:
                photoEditor.setFilterEffect(PhotoFilter.GRAY_SCALE);
                break;
            case 13:
                photoEditor.setFilterEffect(PhotoFilter.LOMISH);
                break;
            case 14:
                photoEditor.setFilterEffect(PhotoFilter.NEGATIVE);
                break;
            case 15:
                photoEditor.setFilterEffect(PhotoFilter.DOCUMENTARY);
                break;
            case 16:
                photoEditor.setFilterEffect(PhotoFilter.DUE_TONE);
                break;
            case 17:
                photoEditor.setFilterEffect(PhotoFilter.FILL_LIGHT);
                break;
            case 18:
                photoEditor.setFilterEffect(PhotoFilter.FISH_EYE);
                break;
            case 19:
                photoEditor.setFilterEffect(PhotoFilter.GRAIN);
                break;
            case 20:
                photoEditor.setFilterEffect(PhotoFilter.TINT);
                break;
            case 21:
                photoEditor.setFilterEffect(PhotoFilter.VIGNETTE);
                break;
            case 22:
                photoEditor.setFilterEffect(PhotoFilter.CROSS_PROCESS);
                break;
            case 23:
                photoEditor.setFilterEffect(PhotoFilter.BLACK_WHITE);
                break;
        }
    }

    public static void setSticker(PhotoEditor photoEditor, int position, Context context) {
        switch (position) {
            case 0:
                Bitmap stickerBitmap1 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_1);
                photoEditor.addImage(stickerBitmap1);
                break;
            case 1:
                Bitmap stickerBitmap2 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_2);
                photoEditor.addImage(stickerBitmap2);
                break;
            case 2:
                Bitmap stickerBitmap3 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_3);
                photoEditor.addImage(stickerBitmap3);
                break;
            case 3:
                Bitmap stickerBitmap4 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_4);
                photoEditor.addImage(stickerBitmap4);
                break;
            case 4:
                Bitmap stickerBitmap5 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_5);
                photoEditor.addImage(stickerBitmap5);
                break;
            case 5:
                Bitmap stickerBitmap6 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_6);
                photoEditor.addImage(stickerBitmap6);
                break;
            case 6:
                Bitmap stickerBitmap7 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_7);
                photoEditor.addImage(stickerBitmap7);
                break;
            case 7:
                Bitmap stickerBitmap8 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_8);
                photoEditor.addImage(stickerBitmap8);
                break;
            case 8:
                Bitmap stickerBitmap9 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_9);
                photoEditor.addImage(stickerBitmap9);
                break;
            case 9:
                Bitmap stickerBitmap10 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_10);
                photoEditor.addImage(stickerBitmap10);
                break;
            case 10:
                Bitmap stickerBitmap11 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_11);
                photoEditor.addImage(stickerBitmap11);
                break;
            case 11:
                Bitmap stickerBitmap12 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_12);
                photoEditor.addImage(stickerBitmap12);
                break;
            case 12:
                Bitmap stickerBitmap13 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_13);
                photoEditor.addImage(stickerBitmap13);
                break;
            case 13:
                Bitmap stickerBitmap14 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_14);
                photoEditor.addImage(stickerBitmap14);
                break;
            case 14:
                Bitmap stickerBitmap15 = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.sticker_15);
                photoEditor.addImage(stickerBitmap15);
        }
    }

}
