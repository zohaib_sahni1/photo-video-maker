package com.photo.video.maker.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import android.widget.TextView
import com.photo.video.maker.R

class NewProcessingDialog(context: Context?, textProcessing: String) : Dialog(context) {
    private var mContext: Context
    private lateinit var proggressBar: ProgressBar
    private lateinit var textView: TextView
    private var textProcessing: String? = textProcessing

    init {
        this.mContext = context!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window!!.setWindowAnimations(R.style.CustomDialogAnimation)
        setContentView(R.layout.new_processing_dailog_layout)
        setCancelable(false)
        proggressBar = findViewById(R.id.progressBar)
        textView = findViewById(R.id.textView)
        textView.text = textProcessing
        val animation = AnimationUtils.loadAnimation(mContext, R.anim.bounce_animation)
        textView.startAnimation(animation)
    }

    fun setTheProgress(progress: Int) {
        proggressBar.progress = progress
    }

    fun dismissIt() {
        try {
            dismiss()
        } catch (e: Exception) {
        }
        if (proggressBar.getVisibility() == View.VISIBLE) {
            proggressBar.setVisibility(View.GONE)
        }
    }
}