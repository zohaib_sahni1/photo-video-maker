package com.photo.video.maker.fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.MyConstants;
import com.photo.video.maker.utils.MyEssentials;

import java.io.IOException;


public class FinalVideoPreviewFragment extends Fragment implements View.OnClickListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    private FragmentManager fragmentManager;
    private Context mContext;
    ConstraintLayout coloredFrame;
    private TextView tvCenterAllign, tvCenterBottom, tvBottomRight, tvBottomLeft, tvTopRight, tvTopLeft;
    private MediaPlayer mediaPlayer;
    VideoView videoView;
    boolean isPlaying = true;
    private String videoPath;
    private String selectionKey;
    private String audioPath = null;
    private int frameColor = 0;
    private int frameThickness = 0;
    private String textForVideo = null;
    private String positionForVideo = null;
    private int colorSelectedForVideo = 0;
    private int textSizeForVideo = 0;

    public static FinalVideoPreviewFragment getInstanceForAll(String enterKey, String videoPath, String audioPath, int frameColor, int frameThickness, String textForVideo, int colorSelectedForVideo, String positionForVideo, int textSizeForVideo) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY, audioPath);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY, frameColor);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY, frameThickness);
        args.putString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO, textForVideo);
        args.putString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO, positionForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO, colorSelectedForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO, textSizeForVideo);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForMusicAndFrame(String enterKey, String videoPath, String audioPath, int frameColor, int frameThickness) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY, audioPath);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY, frameColor);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY, frameThickness);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForMusicAndText(String enterKey, String videoPath, String audioPath, String textForVideo, int colorSelectedForVideo, String positionForVideo, int textSizeForVideo) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY, audioPath);
        args.putString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO, textForVideo);
        args.putString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO, positionForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO, colorSelectedForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO, textSizeForVideo);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForFrameAndText(String enterKey, String videoPath, int frameColor, int frameThickness, String textForVideo, int colorSelectedForVideo, String positionForVideo, int textSizeForVideo) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY, frameColor);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY, frameThickness);
        args.putString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO, textForVideo);
        args.putString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO, positionForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO, colorSelectedForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO, textSizeForVideo);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForMusic(String enterKey, String videoPath, String audioPath) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY, audioPath);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForFrame(String enterKey, String videoPath, int frameColor, int frameThickness) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY, frameColor);
        args.putInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY, frameThickness);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static FinalVideoPreviewFragment getInstanceForVideoText(String enterKey, String videoPath, String textForVideo, int colorSelectedForVideo, String positionForVideo, int textSizeForVideo) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_PREVIEW_KEY, enterKey);
        args.putString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY, videoPath);
        args.putString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO, textForVideo);
        args.putString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO, positionForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO, colorSelectedForVideo);
        args.putInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO, textSizeForVideo);
        FinalVideoPreviewFragment fragment = new FinalVideoPreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final_video_preview, container, false);
        initViews(view);
        initAds(view);
        try {
            if (videoPath != null) {
                videoView.setVideoURI(Uri.parse(videoPath));
            }
            videoView.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        videoView.setOnCompletionListener(this);
        try {
            videoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert getArguments() != null;
        switch (selectionKey) {
            case MyConstants.PREVIEW_WITH_ALL:
                audioPath = getArguments().getString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY);
                frameColor = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY);
                frameThickness = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY);
                textForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO);
                positionForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO);
                colorSelectedForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO);
                textSizeForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO);
                break;
            case MyConstants.PREVIEW_WITH_MUSIC_AND_FRAME:
                audioPath = getArguments().getString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY);
                frameColor = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY);
                frameThickness = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY);
                break;
            case MyConstants.PREVIEW_WITH_MUSIC_AND_TEXT:
                audioPath = getArguments().getString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY);
                textForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO);
                positionForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO);
                colorSelectedForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO);
                textSizeForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO);
                break;
            case MyConstants.PREVIEW_WITH_FRAME_AND_TEXT:
                frameColor = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY);
                frameThickness = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY);
                textForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO);
                positionForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO);
                colorSelectedForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO);
                textSizeForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO);
                break;
            case MyConstants.PREVIEW_WITH_MUSIC:
                audioPath = getArguments().getString(MyConstants.VIDEO_PREVIEW_AUDIO_PATH_KEY);
                break;
            case MyConstants.PREVIEW_WITH_FRAME:
                frameColor = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_COLOR_KEY);
                frameThickness = getArguments().getInt(MyConstants.VIDEO_PREVIEW_FRAME_THICKNESS_KEY);
                break;
            case MyConstants.PREVIEW__WITH_TEXT:
                textForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_TEXT_FOR_VIDEO);
                positionForVideo = getArguments().getString(MyConstants.VIDEO_PREVIEW_POSITION_FOR_VIDEO);
                colorSelectedForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_COLOR_FOR_VIDEO);
                textSizeForVideo = getArguments().getInt(MyConstants.VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO);
                break;
        }
        /*audio player*/
        if (audioPath != null) {
            try {
                mediaPlayer.reset();
            } catch (Exception e) {
                e.printStackTrace();
            }
            initMediaPlayer();
            try {
                mediaPlayer.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        if (frameColor != 0 && frameThickness != 0) {
            float scale = mContext.getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (frameThickness / 2 * scale + 0.5f);
            coloredFrame.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
            coloredFrame.setBackgroundResource(frameColor);
        }
        if (textForVideo != null && colorSelectedForVideo != 0 && positionForVideo != null && textSizeForVideo != 0) {
            switch (positionForVideo) {
                case MyConstants.CENTER_ALLIGN_POSITION:
                    tvCenterAllign.setVisibility(View.VISIBLE);
                    tvCenterAllign.setText(textForVideo);
                    tvCenterAllign.setTextColor(colorSelectedForVideo);
                    tvCenterAllign.setTextSize(textSizeForVideo);
                    break;
                case MyConstants.CENTER_BOTTOM_POSITION:
                    tvCenterBottom.setVisibility(View.VISIBLE);
                    tvCenterBottom.setText(textForVideo);
                    tvCenterBottom.setTextColor(colorSelectedForVideo);
                    tvCenterBottom.setTextSize(textSizeForVideo);
                    break;
                case MyConstants.BOTTOM_RIGHT_POSITION:
                    tvBottomRight.setVisibility(View.VISIBLE);
                    tvBottomRight.setText(textForVideo);
                    tvBottomRight.setTextColor(colorSelectedForVideo);
                    tvBottomRight.setTextSize(textSizeForVideo);
                    break;
                case MyConstants.BOTTOM_LEFT_POSITION:
                    tvBottomLeft.setVisibility(View.VISIBLE);
                    tvBottomLeft.setText(textForVideo);
                    tvBottomLeft.setTextColor(colorSelectedForVideo);
                    tvBottomLeft.setTextSize(textSizeForVideo);
                    break;
                case MyConstants.TOP_RIGHT_POSITION:
                    tvTopRight.setVisibility(View.VISIBLE);
                    tvTopRight.setText(textForVideo);
                    tvTopRight.setTextColor(colorSelectedForVideo);
                    tvTopRight.setTextSize(textSizeForVideo);
                    break;
                case MyConstants.TOP_LEFT_POSITION:
                    tvTopLeft.setVisibility(View.VISIBLE);
                    tvTopLeft.setText(textForVideo);
                    tvTopLeft.setTextColor(colorSelectedForVideo);
                    tvTopLeft.setTextSize(textSizeForVideo);
                    break;
            }
        }
        return view;
    }

    private void initMediaPlayer() {
        if (mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.stop();
                mediaPlayer.reset();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            mediaPlayer.setDataSource(audioPath);
            mediaPlayer.prepare();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initViews(View view) {
        fragmentManager = getActivity().getSupportFragmentManager();
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        view.findViewById(R.id.playPauseBtn).setOnClickListener(this);
        coloredFrame = view.findViewById(R.id.coloredFrame);
        tvCenterAllign = view.findViewById(R.id.tvCenterAllign);
        tvCenterBottom = view.findViewById(R.id.tvCenterBottom);
        tvBottomRight = view.findViewById(R.id.tvBottomRight);
        tvBottomLeft = view.findViewById(R.id.tvBottomLeft);
        tvTopRight = view.findViewById(R.id.tvTopRight);
        tvTopLeft = view.findViewById(R.id.tvTopLeft);
        videoView = view.findViewById(R.id.videoView);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setLooping(false);
        try {
            assert getArguments() != null;
            try {
                videoPath = getArguments().getString(MyConstants.VIDEO_PREVIEW_VIDEO_PATH_KEY);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                selectionKey = getArguments().getString(MyConstants.VIDEO_PREVIEW_KEY);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initAds(View view) {
        BillingHelper billingHelper = new BillingHelper(mContext);
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        LinearLayout fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playPauseBtn:
                if (isPlaying) {
                    try {
                        videoView.pause();
                        mediaPlayer.pause();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isPlaying = false;
                } else {
                    try {
                        videoView.start();
                        mediaPlayer.start();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isPlaying = true;
                }
                break;
            case R.id.doneBtn:
                try {
                    fragmentManager.popBackStack();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        try {
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mediaPlayer.setVolume(1, 1);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0, 0);
            }
        });

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //mp.stop();
        try {
            mediaPlayer.stop();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            fragmentManager.popBackStack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //mediaPlayer.stop();
        try {
            mediaPlayer.release();
            videoView.stopPlayback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //videoView.pause();
        //mediaPlayer.pause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
        //videoView.start();
        //mediaPlayer.start();
    }
}
