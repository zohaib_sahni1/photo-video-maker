package com.photo.video.maker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.NameDialogToSelectedFragmentInterface;


public class VideoNameDialog extends Dialog implements View.OnClickListener {
    private Context mContext;
    private NameDialogToSelectedFragmentInterface callBack;
    private EditText editText;

    public VideoNameDialog(@NonNull Context context, NameDialogToSelectedFragmentInterface callBack) {
        super(context);
        this.mContext = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_video_name_layout);
        TextView addNameBtn = findViewById(R.id.addNameBtn);
        editText = findViewById(R.id.editText);
        findViewById(R.id.skipNameBtn).setOnClickListener(this);
        ImageView backBtn = findViewById(R.id.backBtn);
        addNameBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismissIt();
                break;
            case R.id.addNameBtn:
                if (editText.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please enter some name for Video!", Toast.LENGTH_SHORT).show();
                } else {
                    String videoName = editText.getText().toString();
                    callBack.setNameAndGo(videoName);
                }
                dismissIt();
                break;
            case R.id.skipNameBtn:
                callBack.setNameAndGo("empty");
                dismissIt();
                break;
        }
    }

    private void dismissIt() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
