package com.photo.video.maker.fragments;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.photo.video.maker.R;
import com.photo.video.maker.activities.MainActivity;
import com.photo.video.maker.dialogs.AddFrameDialog;
import com.photo.video.maker.dialogs.AddMusicDialog;
import com.photo.video.maker.dialogs.AddTextToVideoDialog;
import com.photo.video.maker.dialogs.ProcessingDialogNew;
import com.photo.video.maker.interfaces.AddFrameInterface;
import com.photo.video.maker.interfaces.AddMusicInterface;
import com.photo.video.maker.interfaces.DialogTextToVideoInterface;
import com.photo.video.maker.interfaces.ProcessingDialogToVideoViewInterface;
import com.photo.video.maker.utils.*;

import java.io.File;

public class VideoViewFragment extends Fragment implements View.OnClickListener, AddFrameInterface, AddMusicInterface, DialogTextToVideoInterface, MediaPlayer.OnCompletionListener, BottomNavigationView.OnNavigationItemSelectedListener, ProcessingDialogToVideoViewInterface, MediaPlayer.OnPreparedListener {
    VideoView videoView;
    MediaController mediaController;
    String videoPath;
    FFmpeg fFmpeg;
    File videoFile;
    android.support.v4.app.FragmentManager fragmentManager;
    private String[] pathStrings;
    private File[] listFile;
    int listFileLenght;
    File file;
    boolean success;
    String musicPath;
    String enteredKey;
    String fontPath;
    String colorSelected;
    int colorSelectedInt;
    int thickness, textSizeForVideo;
    String textForVideo, positionForVideo, colorSelectedForVideo;
    int colorSelectedIntForVideo;
    Context mContext;
    BottomNavigationView bottomNavigationView;
    boolean shouldGoWithMusic, shouldGoWithFrame, shouldGoWithText;
    String gifPath;

    public static VideoViewFragment getInstance(String path, String enterKey) {
        Bundle args = new Bundle();
        args.putString(MyConstants.VIDEO_VIEW_KEY, path);
        args.putString(MyConstants.VIDEO_VIEW_ENTER, enterKey);
        VideoViewFragment fragment = new VideoViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_view, container, false);
        initViews(view);
        //SaveFrames.saveGifs(R.raw.flower, getActivity());
        initFont();
        //initGif();
        return view;
    }

    private void initViews(View view) {
        try {
            loadFffmpeg();
        } catch (FFmpegNotSupportedException e) {
            Toast.makeText(getActivity(), "Photo Video Maker Framework is not supported bu device!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        fragmentManager = getActivity().getSupportFragmentManager();
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        CardView previewBtn = view.findViewById(R.id.previewBtn);
        previewBtn.setOnClickListener(this);
        bottomNavigationView = view.findViewById(R.id.bottomNavMenu);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        ImageUtils2.saveFont(getActivity());
        videoView = view.findViewById(R.id.videoView);
        mediaController = new MediaController(mContext);
        mediaController.setAnchorView(videoView);
        mediaController.setMediaPlayer(videoView);
        try {
            assert getArguments() != null;
            videoPath = getArguments().getString(MyConstants.VIDEO_VIEW_KEY);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        assert getArguments() != null;
        try {
            enteredKey = getArguments().getString(MyConstants.VIDEO_VIEW_ENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            videoView.setVideoURI(Uri.parse(videoPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            videoView.setMediaController(mediaController);
            videoView.requestFocus();
            videoView.setOnCompletionListener(this);
            videoView.setOnPreparedListener(this);
            videoView.start();
            //mediaController.show(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initFont() {
        try {
            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Photo Video Editor" + File.separator + "Video Editor Fonts" + File.separator);
                success = file.mkdir();
            }
            if (file.exists()) {

                if (file.isDirectory()) {
                    try {
                        listFile = file.listFiles();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (listFile != null) {
                        try {
                            listFileLenght = listFile.length;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        pathStrings = new String[listFileLenght];

                        for (int i = 0; i < listFileLenght; i++) {
                            pathStrings[i] = listFile[i].getAbsolutePath();
                        }
                    }
                }
            }
            if (pathStrings.length > 0) {
                fontPath = pathStrings[0];
            } else {
                SaveFrames.saveFont(R.font.aller_rg, getActivity());
                initFont();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initGif() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Photo Video Editor" + File.separator + "Video Editor Gifs" + File.separator);
            success = file.mkdir();
        }
        if (file.exists()) {

            if (file.isDirectory()) {
                try {
                    listFile = file.listFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (listFile != null) {
                    try {
                        listFileLenght = listFile.length;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pathStrings = new String[listFileLenght];

                    for (int i = 0; i < listFileLenght; i++) {
                        pathStrings[i] = listFile[i].getAbsolutePath();
                    }
                }
            }
        }
        gifPath = pathStrings[0];
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                try {
                    fragmentManager.popBackStack();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.doneBtn:
                videoFile = SaveUtils.createVideoPath(getActivity());
                selectAndExecuteCommand();
                break;
            case R.id.previewBtn:
                checkAndGoForPreview();
                break;
        }
    }

    private void selectAndExecuteCommand() {
        ProcessingDialogNew dialog = new ProcessingDialogNew(mContext, " ", this);
        if (shouldGoWithMusic && shouldGoWithFrame && shouldGoWithText) {
            String command[] = {"-i", videoPath, "-i", musicPath, "-c:v", "libx264", "-c:a", "aac", "-strict", "experimental", "-map", "0:v:0", "-map", "1:a:0", "-shortest", "-vf", "drawtext=fontfile=" + fontPath + ": text='" + textForVideo + "':fontsize=" + textSizeForVideo + ":fontcolor=" + colorSelectedForVideo + ":" + positionForVideo + ", drawbox=x=0:y=0:w=in_w:h=in_h:color=" + colorSelected + ":t=" + thickness, "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithMusic && shouldGoWithFrame) {
            String command[] = {"-i", videoPath, "-i", musicPath, "-c:v", "libx264", "-c:a", "aac", "-strict", "experimental", "-map", "0:v:0", "-map", "1:a:0", "-shortest", "-vf", "drawbox=x=0:y=0:w=in_w:h=in_h:color=" + colorSelected + ":t=" + thickness, "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithMusic && shouldGoWithText) {
            String command[] = {"-i", videoPath, "-i", musicPath, "-c:v", "libx264", "-c:a", "aac", "-strict", "experimental", "-map", "0:v:0", "-map", "1:a:0", "-shortest", "-vf", "drawtext=fontfile=" + fontPath + ": text='" + textForVideo + "':fontsize=" + textSizeForVideo + ":fontcolor=" + colorSelectedForVideo + ":" + positionForVideo, "-c:v", "libx264", "-c:a", "copy", "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithText && shouldGoWithFrame) {
            String command[] = {"-i", videoPath, "-vf", "drawtext=fontfile=" + fontPath + ": text='" + textForVideo + "':fontsize=" + textSizeForVideo + ":fontcolor=" + colorSelectedForVideo + ":" + positionForVideo + ", drawbox=x=0:y=0:w=in_w:h=in_h:color=" + colorSelected + ":t=" + thickness, "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithMusic) {
            String command[] = {"-i", videoPath, "-i", musicPath, "-c:v", "libx264", "-c:a", "aac", "-strict", "experimental", "-map", "0:v:0", "-map", "1:a:0", "-shortest", "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithFrame) {
            String command[] = {"-i", videoPath, "-vf", "drawbox=x=0:y=0:w=in_w:h=in_h:color=" + colorSelected + ":t=" + thickness, "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else if (shouldGoWithText) {
            String command[] = {"-i", videoPath, "-vf", "drawtext=fontfile=" + fontPath + ": text='" + textForVideo + "':fontsize=" + textSizeForVideo + ":fontcolor=" + colorSelectedForVideo + ":" + positionForVideo, "-c:v", "libx264", "-c:a", "copy", "-preset", "ultrafast", videoFile.getAbsolutePath()};
            //String command[] = {"-i", videoPath, "-ignore_loop", "0", "-i", gifPath, "-filter_complex", "overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2:shortest=1", "-c:v", "libx264", "-c:a", "copy", "-preset", "ultrafast", videoFile.getAbsolutePath()};
            FFmpegHelper.executeFfmpegMusicCommands(mContext, command, fFmpeg, videoFile.getAbsolutePath(), fragmentManager, dialog);
        } else {
            try {
                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (fFmpeg.isFFmpegCommandRunning()) {
            try {
                fFmpeg.killRunningProcesses();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    public void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            fFmpeg.killRunningProcesses();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissCurrentDialog(Dialog dialog) {
        if (dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadFffmpeg() throws FFmpegNotSupportedException {
        if (fFmpeg == null) {
            fFmpeg = FFmpeg.getInstance(getActivity());
        }
        fFmpeg.loadBinary(new LoadBinaryResponseHandler() {
            @Override
            public void onFailure() {
                super.onFailure();
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
            }
        });
    }

    @Override
    public void setTextToVideo(String text, String position, String color, int colorSelectedInt, int textSize, boolean shouldGo) {
        textForVideo = text;
        positionForVideo = position;
        colorSelectedForVideo = color;
        colorSelectedIntForVideo = colorSelectedInt;
        textSizeForVideo = textSize;
        shouldGoWithText = shouldGo;
    }

    @Override
    public void setFrameColor(String frameColor, int thickNew, int colorNew, boolean shouldGo) {
        colorSelected = frameColor;
        thickness = thickNew;
        colorSelectedInt = colorNew;
        shouldGoWithFrame = shouldGo;
    }

    @Override
    public void setMusicPathToFfmpeg(String path, boolean shouldGo) {
        musicPath = path;
        shouldGoWithMusic = shouldGo;
    }

    private void checkAndGoForPreview() {
        if (shouldGoWithMusic && shouldGoWithFrame && shouldGoWithText) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForAll(MyConstants.PREVIEW_WITH_ALL, videoPath, musicPath, colorSelectedInt, thickness, textForVideo, colorSelectedIntForVideo, positionForVideo, textSizeForVideo);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithMusic && shouldGoWithFrame) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForMusicAndFrame(MyConstants.PREVIEW_WITH_MUSIC_AND_FRAME, videoPath, musicPath, colorSelectedInt, thickness);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithMusic && shouldGoWithText) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForMusicAndText(MyConstants.PREVIEW_WITH_MUSIC_AND_TEXT, videoPath, musicPath, textForVideo, colorSelectedIntForVideo, positionForVideo, textSizeForVideo);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithText && shouldGoWithFrame) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForFrameAndText(MyConstants.PREVIEW_WITH_FRAME_AND_TEXT, videoPath, colorSelectedInt, thickness, textForVideo, colorSelectedIntForVideo, positionForVideo, textSizeForVideo);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithMusic) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForMusic(MyConstants.PREVIEW_WITH_MUSIC, videoPath, musicPath);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithFrame) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForFrame(MyConstants.PREVIEW_WITH_FRAME, videoPath, colorSelectedInt, thickness);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else if (shouldGoWithText) {
            FinalVideoPreviewFragment previewFragment = FinalVideoPreviewFragment.getInstanceForVideoText(MyConstants.PREVIEW__WITH_TEXT, videoPath, textForVideo, colorSelectedIntForVideo, positionForVideo, textSizeForVideo);
            fragmentManager.beginTransaction().replace(R.id.frameLayout, previewFragment).addToBackStack(null).commitAllowingStateLoss();
        } else {
            Toast.makeText(mContext, "You must select an option for previewing!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        try {
            videoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        navigationItemClicked(item.getItemId());
        return true;
    }

    private void navigationItemClicked(int id) {
        switch (id) {
            case R.id.selectMusicMenuBtn:
                AddMusicDialog addMusicDialog = new AddMusicDialog(mContext, this);
                addMusicDialog.show();
                break;
            case R.id.selectFrameMenuBtn:
                AddFrameDialog addFrameDialog = new AddFrameDialog(mContext, this);
                addFrameDialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialogAnimation;
                addFrameDialog.show();
                break;
            case R.id.selectTextMenuBtn:
                AddTextToVideoDialog addTextToVideoDialogDialog = new AddTextToVideoDialog(mContext, this);
                addTextToVideoDialogDialog.show();
                break;
        }
    }

    @Override
    public void doAction() {
        Intent toMainIntent = new Intent(getActivity(), MainActivity.class);
        toMainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        toMainIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        toMainIntent.putExtra(MyConstants.FRAGMENT_SELECTION_KEY, "main_view_fragment");
        toMainIntent.putExtra(MyConstants.FRAGMENT_SELECTION_PATH, "No Path");
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, toMainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        try {
            if (mContext != null && !getActivity().isFinishing()) {
                mediaController.show(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
