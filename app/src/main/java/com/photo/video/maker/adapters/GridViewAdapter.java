package com.photo.video.maker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;
import com.photo.video.maker.fragments.SelectImageFragment;
import com.photo.video.maker.interfaces.CounterTextInterface;
import com.photo.video.maker.interfaces.SlidingAdapterToGridAdapter;
import com.photo.video.maker.model.ImagesModel;
import com.photo.video.maker.model.NewModel;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter<ImagesModel> implements SlidingAdapterToGridAdapter {
    Context context;
    private ViewHolder viewHolder;
    private ArrayList<ImagesModel> modelList;
    private ArrayList<String> selecetdImages = new ArrayList<>();
    private int positionFolder;
    private SlidingViewAdapter slidingViewAdapter;
    private SelectImageFragment fragment;
    private ArrayList<NewModel> selectedWithCounter = new ArrayList<>();
    private CounterTextInterface counterTextInterface;

    public GridViewAdapter(Context context, ArrayList<ImagesModel> modelList, int positionFolder, SlidingViewAdapter slidingViewAdapter, CounterTextInterface counterTextInterface) {
        super(context, R.layout.grid_item_layout, modelList);
        this.modelList = modelList;
        this.context = context;
        this.positionFolder = positionFolder;
        this.slidingViewAdapter = slidingViewAdapter;
        fragment = new SelectImageFragment();
        this.counterTextInterface = counterTextInterface;
    }

    @Override
    public int getCount() {
        return modelList.get(positionFolder).getAl_imagepath().size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        if (modelList.get(positionFolder).getAl_imagepath().size() > 0) {
            return modelList.get(positionFolder).getAl_imagepath().size();
        } else {
            return 1;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final String currentPath;
        viewHolder = new ViewHolder();
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item_layout, parent, false);
        viewHolder.imageView = convertView.findViewById(R.id.imageView);
        viewHolder.tickBtn = convertView.findViewById(R.id.tickBtn);
        viewHolder.countView = convertView.findViewById(R.id.countView);
        viewHolder.parentLayout = convertView.findViewById(R.id.parentLayout);
        currentPath = modelList.get(positionFolder).getAl_imagepath().get(position);
        Glide.with(context).load("file://" + currentPath)
                .into(viewHolder.imageView);
        if (selecetdImages.contains(currentPath)) {
            viewHolder.tickBtn.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tickBtn.setVisibility(View.GONE);
        }
        for (int i = 0; i < selectedWithCounter.size(); i++) {
            if (selectedWithCounter.get(i).getItemPath().equalsIgnoreCase(currentPath)) {
                viewHolder.countView.setText(String.valueOf(selectedWithCounter.get(i).getItemCount()));
            }
        }
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPathPresentBefore = false;
                for (int i = 0; i < selectedWithCounter.size(); i++) {
                    if (selectedWithCounter.get(i).getItemPath().equalsIgnoreCase(currentPath)) {
                        selectedWithCounter.get(i).setItemCount(selectedWithCounter.get(i).getItemCount() + 1);
                        isPathPresentBefore = true;
                    }
                }
                if (!isPathPresentBefore) {
                    NewModel newModel = new NewModel();
                    newModel.setItemPath(currentPath);
                    newModel.setItemCount(newModel.getItemCount() + 1);
                    selectedWithCounter.add(newModel);
                }
                selecetdImages.add(currentPath);
                counterTextInterface.setCount(selecetdImages.size());
                viewHolder.tickBtn.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                fragment.getDataList().add(currentPath);
                if (slidingViewAdapter == null) {
                    slidingViewAdapter = new SlidingViewAdapter(context, fragment.getDataList(), GridViewAdapter.this);
                } else {
                    slidingViewAdapter.setPathDataList(fragment.getDataList());
                }
                fragment.getSlidingRecyclerView().setAdapter(slidingViewAdapter);
                slidingViewAdapter.notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public void doChanges(boolean flag, int position, String path) {
        if (flag) {
            for (int i = 0; i < selectedWithCounter.size(); i++) {
                if (selectedWithCounter.get(i).getItemPath().equalsIgnoreCase(path)) {
                    selectedWithCounter.get(i).setItemCount(selectedWithCounter.get(i).getItemCount() - 1);
                    viewHolder.countView.setText(String.valueOf(selectedWithCounter.get(i).getItemCount()));
                    notifyDataSetChanged();
                }
            }
            if (selecetdImages.contains(path)) {
                viewHolder.tickBtn.setVisibility(View.GONE);
                selecetdImages.remove(position);
                counterTextInterface.setCount(selecetdImages.size());
                notifyDataSetChanged();
            }
        }
    }

    public static class ViewHolder {
        ImageView imageView;
        ImageView tickBtn;
        TextView countView;
        RelativeLayout parentLayout;
    }

    public void setModelList(ArrayList<ImagesModel> modelList) {
        this.modelList = modelList;
    }

    public void setPositionFolder(int positionFolder) {
        this.positionFolder = positionFolder;
    }
}
