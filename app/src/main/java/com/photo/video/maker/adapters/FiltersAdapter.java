package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;
import com.photo.video.maker.model.FilterModel;
import com.photo.video.maker.utils.AnimationsUtils;

import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.MyViewHolder> {
    private List<FilterModel> filterModelList;
    Context context;
    private int lastPosition;

    public FiltersAdapter(Context context, List<FilterModel> filterModelList) {
        this.context = context;
        this.filterModelList = filterModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_row_item_new, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            FilterModel item = filterModelList.get(position);
            Glide.with(context).load(item.getImageDrawable()).into(holder.imageView);
            holder.textView.setText(item.getString());
            if (holder.getAdapterPosition() == lastPosition) {
                holder.tickBtn.setVisibility(View.VISIBLE);
            } else {
                holder.tickBtn.setVisibility(View.GONE);
            }
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastPosition = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return filterModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private ImageView tickBtn;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.filterView);
            textView = itemView.findViewById(R.id.filterName);
            tickBtn = itemView.findViewById(R.id.tickBtn);
            AnimationsUtils.setScaleAnimation(itemView);
        }
    }

}
