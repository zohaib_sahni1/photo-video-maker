package com.photo.video.maker.utils;

public class MyConstants {
    public static final String SELECTED_IMAGE_KEY = "SELECTED_IMAGE_KEY";
    public static final String SELECTED_IMAGE_FRAGMENT_ASYNCH = "SELECTED_IMAGE_FRAGMENT_ASYNCH";
    public static final String EDITOR_IMAGE_KEY = "EDITOR_IMAGE_KEY";
    public static final String EDITOR_LIST_KEY = "EDITOR_LIST_KEY";
    public static final String EDITOR_POSITION_KEY = "EDITOR_POSITION_KEY";
    public static final String VIDEO_VIEW_KEY = "VIDEO_VIEW_KEY";
    public static final String VIDEO_VIEW_MUSIC_KEY = "VIDEO_VIEW_MUSIC_KEY";
    public static final String VIDEO_VIEW_MUSIC_ACTION = "VIDEO_VIEW_MUSIC_ACTION";
    public static final String SAVED_IMAGE_VIEW_PATH_KEY = "SAVED_IMAGE_VIEW_PATH_KEY";
    public static final String SAVED_IMAGE_VIEW_NAME_KEY = "SAVED_IMAGE_VIEW_NAME_KEY";
    public static final String SAVED_VIDEO_VIEW_PATH_KEY = "SAVED_VIDEO_VIEW_PATH_KEY";
    public static final String SAVED_VIDEO_VIEW_NAME_KEY = "SAVED_VIDEO_VIEW_NAME_KEY";
    public static final String BOTTOM_RIGHT_POSITION = "x=w-tw-300:y=h-th-300";
    public static final String TOP_RIGHT_POSITION = "x=w-tw-300:y=300";
    public static final String TOP_LEFT_POSITION = "x=300:y=300";
    public static final String BOTTOM_LEFT_POSITION = "x=300:h-th-300";
    public static final String CENTER_BOTTOM_POSITION = "x=(main_w/2-text_w/2):y=main_h-(text_h*2)";
    public static final String CENTER_ALLIGN_POSITION = "x=(w-text_w)/2: y=(h-text_h)/3";
    public static final String VIDEO_VIEW_ENTER = "VIDEO_VIEW_ENTER";
    public static final String VIDEO_VIEW_ENTER_WITH_CREATED = "VIDEO_VIEW_ENTER_WITH_CREATED";
    public static final String VIDEO_VIEW_ENTER_WITH_EDITED = "VIDEO_VIEW_ENTER_WITH_EDITED";
    public static final String FRAGMENT_SELECTION_KEY = "FRAGMENT_SELECTION_KEY";
    public static final String FRAGMENT_SELECTION_PATH = "FRAGMENT_SELECTION_PATH";
    public static final String TO_MUSIC_ACTION_SIMPLE = "TO_MUSIC_ACTION_SIMPLE";
    public static final String TO_MUSIC_ACTION_NOTIFICATION = "TO_MUSIC_ACTION_NOTIFICATION";
    public static final String VIDEO_PREVIEW_KEY = "VIDEO_PREVIEW_KEY";
    public static final String VIDEO_PREVIEW_VIDEO_PATH_KEY = "VIDEO_PREVIEW_VIDEO_PATH_KEY";
    public static final String VIDEO_PREVIEW_AUDIO_PATH_KEY = "VIDEO_PREVIEW_AUDIO_PATH_KEY";
    public static final String VIDEO_PREVIEW_FRAME_COLOR_KEY = "VIDEO_PREVIEW_FRAME_COLOR_KEY";
    public static final String VIDEO_PREVIEW_FRAME_THICKNESS_KEY = "VIDEO_PREVIEW_FRAME_THICKNESS_KEY";
    public static final String VIDEO_PREVIEW_TEXT_FOR_VIDEO = "VIDEO_PREVIEW_TEXT_FOR_VIDEO";
    public static final String VIDEO_PREVIEW_POSITION_FOR_VIDEO = "VIDEO_PREVIEW_POSITION_FOR_VIDEO";
    public static final String VIDEO_PREVIEW_COLOR_FOR_VIDEO = "VIDEO_PREVIEW_COLOR_FOR_VIDEO";
    public static final String VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO = "VIDEO_PREVIEW_TEXT_SIZE_FOR_VIDEO";
    public static final String PREVIEW_WITH_MUSIC = "PREVIEW_WITH_MUSIC";
    public static final String PREVIEW_WITH_FRAME = "PREVIEW_WITH_FRAME";
    public static final String PREVIEW__WITH_TEXT = "PREVIEW__WITH_TEXT";
    public static final String PREVIEW_WITH_MUSIC_AND_FRAME = "PREVIEW_WITH_MUSIC_AND_FRAME";
    public static final String PREVIEW_WITH_MUSIC_AND_TEXT = "PREVIEW_WITH_MUSIC_AND_TEXT";
    public static final String PREVIEW_WITH_FRAME_AND_TEXT = "PREVIEW_WITH_FRAME_AND_TEXT";
    public static final String PREVIEW_WITH_ALL = "PREVIEW_WITH_ALL";
    public static final int STICKER_GALLERY_PICKER_INTENT = 2;
    public static final int VIDEO_PICK_INTENT_KEY = 1;

}
