package com.photo.video.maker.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Surface;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class SlideEncoder {
    public static final int WIDTH = 512;
    public static final int HEIGHT = 512;
    private static final String MIME_TYPE_NEW = MediaFormat.MIMETYPE_VIDEO_AVC;
    private static final int MY_FRAME_INTERVAL = 3;
    private MediaCodec.BufferInfo myBufferInfo;
    private MediaCodec mediaCodecEncoder;
    private Surface myInputSurface;
    private MediaMuxer mMuxer;
    private int mTrackIndex;
    private boolean mMuxerStarted;
    private long mFakePts;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void prepareEncoder(File outputFile) throws IOException {
        myBufferInfo = new MediaCodec.BufferInfo();
        MediaFormat format = null;
        try {
            format = MediaFormat.createVideoFormat(MIME_TYPE_NEW, WIDTH, HEIGHT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        } catch (Exception e) {
            e.printStackTrace();
        }
        format.setInteger(MediaFormat.KEY_BIT_RATE, MySlideApplication.Companion.getBIT_RATE());
        format.setInteger(MediaFormat.KEY_FRAME_RATE, MySlideApplication.Companion.getFRAME_PER_SEC());
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, MY_FRAME_INTERVAL);
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        try {
            mediaCodecEncoder = MediaCodec.createEncoderByType(MIME_TYPE_NEW);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mediaCodecEncoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            myInputSurface = mediaCodecEncoder.createInputSurface();
            mediaCodecEncoder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMuxer = new MediaMuxer(outputFile.toString(),
                MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);

        mTrackIndex = -1;
        mMuxerStarted = false;
    }

    public void releaseEncoder() {
        if (mediaCodecEncoder != null) {
            try {
                mediaCodecEncoder.stop();
                mediaCodecEncoder.release();
                mediaCodecEncoder = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (myInputSurface != null) {
            try {
                myInputSurface.release();
                myInputSurface = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (mMuxer != null) {
                try {
                    mMuxer.stop();
                    mMuxer.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mMuxer = null;
            }
        } catch (IllegalStateException e) {
            mMuxer = null;
        } catch (Exception e) {
            e.printStackTrace();
            mMuxer = null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void drainEncoder(boolean endOfStream) {
        final int TIMEOUT_USEC = 10000;

        if (endOfStream) {
            try {
                mediaCodecEncoder.signalEndOfInputStream();//crash for every image
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ByteBuffer[] encoderOutputBuffers = new ByteBuffer[0];
        try {
            encoderOutputBuffers = mediaCodecEncoder.getOutputBuffers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (true) {
            int encoderStatus = 0;
            try {
                encoderStatus = mediaCodecEncoder.dequeueOutputBuffer(myBufferInfo, TIMEOUT_USEC);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if (!endOfStream) {
                    break;
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                encoderOutputBuffers = mediaCodecEncoder.getOutputBuffers();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                if (mMuxerStarted) {
                    throw new RuntimeException("format changed twice");
                }
                MediaFormat newFormat = mediaCodecEncoder.getOutputFormat();
                mTrackIndex = mMuxer.addTrack(newFormat);
                try {
                    mMuxer.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mMuxerStarted = true;
            } else if (encoderStatus < 0) {
            } else {
                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                if (encodedData == null) {
                    throw new RuntimeException("encoderOutputBuffer " + encoderStatus +
                            " was null");
                }

                if ((myBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    myBufferInfo.size = 0;
                }

                if (myBufferInfo.size != 0) {
                    if (!mMuxerStarted) {
                        throw new RuntimeException("muxer hasn't started");
                    }
                    try {
                        encodedData.position(myBufferInfo.offset);
                        encodedData.limit(myBufferInfo.offset + myBufferInfo.size);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    myBufferInfo.presentationTimeUs = mFakePts;
                    long timeStampLength = 1000000L / MySlideApplication.Companion.getFRAME_PER_SEC();
                    mFakePts += timeStampLength;
                    try {
                        mMuxer.writeSampleData(mTrackIndex, encodedData, myBufferInfo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    mediaCodecEncoder.releaseOutputBuffer(encoderStatus, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ((myBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                    } else {
                    }
                    break;
                }
            }
        }
    }

    public void generateFrame(Bitmap prevBm, Bitmap curBm) {
        Canvas canvas = null;
        try {
            canvas = myInputSurface.lockCanvas(null);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Surface.OutOfResourcesException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IllegalStateException s) {
            s.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception s) {
            s.printStackTrace();
        }

        try {
            MySlideShow.Companion.draw(canvas, prevBm, curBm);
        } finally {
            if (myInputSurface != null && canvas != null) {
                try {
                    myInputSurface.unlockCanvasAndPost(canvas);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}