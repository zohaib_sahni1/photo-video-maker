package com.photo.video.maker.interfaces;

public interface DialogTextToVideoInterface {
    void setTextToVideo(String text, String position,String color,int colorInt,int textSize,boolean shouldGo);
}
