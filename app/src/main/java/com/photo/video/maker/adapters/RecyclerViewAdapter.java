package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;
import com.photo.video.maker.model.ImagesModel;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    Context context;
    private ArrayList<ImagesModel> imagesModelList;
    private int lastClickedPosition = 0;

    public RecyclerViewAdapter(Context context, ArrayList<ImagesModel> imagesModelList) {
        this.context = context;
        this.imagesModelList = imagesModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_layout, parent, false);
        return new MyViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        if (holder.getAdapterPosition() != RecyclerView.NO_POSITION) {
            holder.textView.setText(imagesModelList.get(holder.getAdapterPosition()).getStr_folder());
            Glide.with(context).load("file://" + imagesModelList.get(holder.getAdapterPosition()).getAl_imagepath()
                    .get(0)).into(holder.imageView);
            if (holder.getAdapterPosition() == lastClickedPosition) {
                holder.imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.recycelr_shape_clicked));
            } else {
                holder.imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.recycler_shape));
            }
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastClickedPosition = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return imagesModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
        }
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


}
