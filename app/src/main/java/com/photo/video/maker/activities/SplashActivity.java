package com.photo.video.maker.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;
import com.photo.video.maker.BuildConfig;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.AnalyticsConstants;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.MyEssentials;

public class SplashActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd = null;
    BillingHelper billingHelper;
    ProgressBar bar;
    int SPLASH_TIMEOUT = 6000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        AdSettings.addTestDevice("dbf33f70-5ed6-4a2d-9399-7bf1f427be58");
        FacebookSdk.sdkInitialize(this);
        AudienceNetworkAds.initialize(this);
        FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.APPLICATION_ID);
        bar = findViewById(R.id.bar);
        bar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.latest_bg), PorterDuff.Mode.MULTIPLY);
        billingHelper = new BillingHelper(this);
        if (billingHelper.shouldShowAds()) {
            mInterstitialAd = MyEssentials.loadAdMobInterstitialAd(this);
        }
    }

    private void moveForward() {
        if (bar.getVisibility() == View.VISIBLE) {
            bar.setVisibility(View.GONE);
        }
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Intent intent = new Intent(SplashActivity.this, PrivacyActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                    MyEssentials.logEvents(AnalyticsConstants.SPLASH_ACTIVITY_TO_MAIN_ACTIVITY, getApplicationContext());
                }
            });
        } else {
            Intent intent = new Intent(SplashActivity.this, PrivacyActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moveForward();
            }
        }, SPLASH_TIMEOUT);
    }
}
