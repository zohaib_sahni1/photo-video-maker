package com.photo.video.maker.interfaces;

public interface AddFrameInterface {
    void setFrameColor(String frameColor,int thickness,int colorSelectedInt,boolean shouldGo);
}
