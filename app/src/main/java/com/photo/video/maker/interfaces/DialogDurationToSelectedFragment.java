package com.photo.video.maker.interfaces;

public interface DialogDurationToSelectedFragment {
    void setDuration(int duration);
}
