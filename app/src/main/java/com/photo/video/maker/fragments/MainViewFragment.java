package com.photo.video.maker.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.*;

import java.io.File;


public class MainViewFragment extends Fragment implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    ImageView drawerBtn, removeAdBtn;
    ConstraintLayout topLayout;
    SelectImageFragment selectImageFragment;
    CreationsFragment creationsFragment;
    EditVideoFragment editVideoFragment;
    android.support.v4.app.FragmentManager fragmentManager;
    Context mContext;
    LinearLayout fbAdContainer;
    InterstitialAd mInterstitialAd;
    com.facebook.ads.InterstitialAd fbInterstitialAd;
    BillingHelper billingHelper;
    BillingProcessor billingProcessor;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private String[] videoPathStrings;
    private String[] videoNamesString;
    private Bitmap[] thumbnailString;
    private File[] listFile;
    int listFileLenght;
    File file;
    boolean success;
    ImageView videoThn1, videoThn2, videoThn3, videoThn4;
    ImageView play1, play2, play3, play4;
    String videoPath1 = "path", videoPath2 = "path", videoPath3 = "path", videoPath4 = "path";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_view_new, container, false);
        billingProcessor = new BillingProcessor(mContext, getResources().getString(R.string.billing_key), myBillingHandler);
        try {
            readLatestVideos();
        } catch (Exception e) {
            e.printStackTrace();
        }
        drawerBtn = view.findViewById(R.id.drawerBtn);
        removeAdBtn = view.findViewById(R.id.removeAdBtn);
        removeAdBtn.setOnClickListener(this);
        topLayout = view.findViewById(R.id.topLayout);
        drawerLayout = view.findViewById(R.id.drawerLayout);
        navigationView = view.findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        drawerBtn.setOnClickListener(this);
        videoThn1 = view.findViewById(R.id.videoImage1);
        videoThn2 = view.findViewById(R.id.videoImage2);
        videoThn3 = view.findViewById(R.id.videoImage3);
        videoThn4 = view.findViewById(R.id.videoImage4);
        play1 = view.findViewById(R.id.play1);
        play2 = view.findViewById(R.id.play2);
        play3 = view.findViewById(R.id.play3);
        play4 = view.findViewById(R.id.play4);
        fragmentManager = getActivity().getSupportFragmentManager();
        selectImageFragment = new SelectImageFragment();
        creationsFragment = new CreationsFragment();
        editVideoFragment = new EditVideoFragment();
        billingHelper = new BillingHelper(getActivity());
        view.findViewById(R.id.createVideoBtn).setOnClickListener(this);
        view.findViewById(R.id.editVideoBtn).setOnClickListener(this);
        view.findViewById(R.id.myCreationsBtn).setOnClickListener(this);
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_medium), AdSize.RECTANGLE_HEIGHT_250);
        fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
            mInterstitialAd = MyEssentials.loadAdMobInterstitialAd(mContext);
            fbInterstitialAd = MyEssentials.loadFbInterstitialAd(mContext);
            removeAdBtn.setVisibility(View.VISIBLE);
        }
        try {
            showImages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void showImages() {
        if (videoPathStrings.length == 1) {
            Glide.with(mContext).load(thumbnailString[listFileLenght - 1]).into(videoThn1);
            videoThn1.setOnClickListener(this);
            play1.setVisibility(View.VISIBLE);
            videoPath1 = videoPathStrings[listFileLenght - 1];
        } else if (videoPathStrings.length == 2) {
            Glide.with(mContext).load(thumbnailString[listFileLenght - 1]).into(videoThn1);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 2]).into(videoThn2);
            videoThn1.setOnClickListener(this);
            videoThn2.setOnClickListener(this);
            play1.setVisibility(View.VISIBLE);
            play2.setVisibility(View.VISIBLE);
            videoPath1 = videoPathStrings[listFileLenght - 1];
            videoPath2 = videoPathStrings[listFileLenght - 2];
        } else if (videoPathStrings.length == 3) {
            Glide.with(mContext).load(thumbnailString[listFileLenght - 1]).into(videoThn1);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 2]).into(videoThn2);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 3]).into(videoThn3);
            videoThn1.setOnClickListener(this);
            videoThn2.setOnClickListener(this);
            videoThn3.setOnClickListener(this);
            play1.setVisibility(View.VISIBLE);
            play2.setVisibility(View.VISIBLE);
            play3.setVisibility(View.VISIBLE);
            videoPath1 = videoPathStrings[listFileLenght - 1];
            videoPath2 = videoPathStrings[listFileLenght - 2];
            videoPath3 = videoPathStrings[listFileLenght - 3];
        } else if (videoPathStrings.length >= 4) {
            Glide.with(mContext).load(thumbnailString[listFileLenght - 1]).into(videoThn1);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 2]).into(videoThn2);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 3]).into(videoThn3);
            Glide.with(mContext).load(thumbnailString[listFileLenght - 4]).into(videoThn4);
            videoThn1.setOnClickListener(this);
            videoThn2.setOnClickListener(this);
            videoThn3.setOnClickListener(this);
            videoThn4.setOnClickListener(this);
            play1.setVisibility(View.VISIBLE);
            play2.setVisibility(View.VISIBLE);
            play3.setVisibility(View.VISIBLE);
            play4.setVisibility(View.VISIBLE);
            videoPath1 = videoPathStrings[listFileLenght - 1];
            videoPath2 = videoPathStrings[listFileLenght - 2];
            videoPath3 = videoPathStrings[listFileLenght - 3];
            videoPath4 = videoPathStrings[listFileLenght - 4];
        }
    }

    @SuppressLint("InflateParams")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.drawerBtn:
                drawerLayout.openDrawer(navigationView);
                break;
            case R.id.removeAdBtn:
                billingProcessor.purchase(getActivity(), getString(R.string.ads_remove_key));
                break;
            case R.id.createVideoBtn:
                setFragment(selectImageFragment);
                break;
            case R.id.myCreationsBtn:
                fragmentManager.beginTransaction().replace(R.id.frameLayout, creationsFragment).addToBackStack(null).commitAllowingStateLoss();
                break;
            case R.id.editVideoBtn:
                setFragment(editVideoFragment);
                break;
            case R.id.videoImage1:
                openPlayerActivity(videoPath1, "Video");
                break;
            case R.id.videoImage2:
                openPlayerActivity(videoPath2, "Video");
                break;
            case R.id.videoImage3:
                openPlayerActivity(videoPath3, "Video");
                break;
            case R.id.videoImage4:
                openPlayerActivity(videoPath4, "Video");
                break;
        }
    }

    private void openPlayerActivity(String path, String name) {
        SavedVideoViewFragment fragment = SavedVideoViewFragment.Companion.getInstance(path, name);
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
    }

    public void setFragment(final Fragment fragment) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(mContext, "Restart the application and grant all the permissions!", Toast.LENGTH_SHORT).show();
        } else {
            if (MyEssentials.isMediation) {
                if (fbInterstitialAd != null && fbInterstitialAd.isAdLoaded() && !fbInterstitialAd.isAdInvalidated()) {
                    fbInterstitialAd.show();
                    fbInterstitialAd.setAdListener(new FbAdListener() {
                        @Override
                        public void onInterstitialDismissed(Ad ad) {
                            super.onInterstitialDismissed(ad);
                            MyEssentials.isMediation = false;
                            MyEssentials.setTimer(5000);
                            fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
                        }

                        @Override
                        public void onAdClicked(Ad ad) {
                            super.onAdClicked(ad);
                            MyEssentials.isMediation = false;
                            MyEssentials.setTimer(15000);
                            MyEssentials.logEvents(AnalyticsConstants.MENU_ACTIVITY_BUTTON_CLICKED, getActivity());
                        }
                    });
                } else if (mInterstitialAd.isLoaded() && mInterstitialAd != null) {
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            MyEssentials.isMediation = false;
                            MyEssentials.setTimer(5000);
                            fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
                        }

                        @Override
                        public void onAdClicked() {
                            super.onAdClicked();
                            MyEssentials.isMediation = false;
                            MyEssentials.setTimer(15000);
                            MyEssentials.logEvents(AnalyticsConstants.MENU_ACTIVITY_BUTTON_CLICKED, getActivity());
                        }
                    });
                } else {
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
                }
            } else {
                fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).addToBackStack(null).commitAllowingStateLoss();
            }
        }
    }

    private void readLatestVideos() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Environment.getExternalStorageState().equals(
                        Environment.MEDIA_MOUNTED)) {
                    file = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "Photo Video Editor" + File.separator + "Video Editor Videos" + File.separator);
                    success = file.mkdir();
                }
                if (file.exists()) {

                    if (file.isDirectory()) {
                        try {
                            listFile = file.listFiles();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (listFile != null) {
                            try {
                                listFileLenght = listFile.length;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            videoPathStrings = new String[listFileLenght];
                            videoNamesString = new String[listFileLenght];
                            thumbnailString = new Bitmap[listFileLenght];

                            for (int i = 0; i < listFileLenght; i++) {
                                videoPathStrings[i] = listFile[i].getAbsolutePath();
                                videoNamesString[i] = listFile[i].getName();
                                thumbnailString[i] = ThumbnailUtils.createVideoThumbnail(videoPathStrings[i], MediaStore.Video.Thumbnails.MICRO_KIND);
                            }
                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Empty", Toast.LENGTH_SHORT).show();
                    videoPathStrings = null;
                    videoNamesString = null;
                }

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        CheckPermissions.checkPermissionAndGo(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (billingProcessor != null) {
            billingProcessor.release();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        FFmpegHelper.cancelNotification();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.tv2:
                try {
                    Intent shareAppIntent = new Intent(Intent.ACTION_SEND);
                    shareAppIntent.setType("text/plane");
                    String shareSub = "Check out this application on play store!";
                    String shareBody = getString(R.string.share_application);
                    shareAppIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
                    shareAppIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(shareAppIntent, "Share using..."));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv3:
                Intent intentRateApp = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.rate_us)));
                startActivity(intentRateApp);
                break;
            case R.id.tv4:
                Intent intentMoreApps = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.more_applications)));
                startActivity(intentMoreApps);
                break;
            case R.id.tv5:
                Intent intentPolicy = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy_link)));
                startActivity(intentPolicy);
                break;
        }
        drawerLayout.closeDrawers();
        return true;
    }

    BillingProcessor.IBillingHandler myBillingHandler = new BillingProcessor.IBillingHandler() {
        @Override
        public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        }

        @Override
        public void onPurchaseHistoryRestored() {
        }

        @Override
        public void onBillingError(int errorCode, @Nullable Throwable error) {
        }

        @Override
        public void onBillingInitialized() {
        }
    };
}
