package com.photo.video.maker.interfaces;

import android.widget.ImageView;

public interface GetImageViewInterface {
    ImageView getImageViewAdapter();
}
