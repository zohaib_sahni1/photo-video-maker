package com.photo.video.maker.utils;

import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

 public class SaveFrames {

    public static void saveFont(int id, final Activity activity) {
        File fileMain = null;
        try {
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show();
            } else {
                fileMain = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Photo Video Editor" + File.separator + "Video Editor Fonts" + File.separator);
                boolean success = fileMain.mkdirs();
            }
            File file1 = new File(fileMain, "aller_rg" + System.currentTimeMillis() + ".ttf");
            try {
                InputStream is = activity.getResources().openRawResource(id);
                OutputStream os = new FileOutputStream(file1);
                byte[] data = new byte[is.available()];
                is.read(data);
                os.write(data);
                is.close();
                os.close();
            } catch (IOException e) {
                Toast.makeText(activity, "Exception", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e1) {
            e1.printStackTrace();}
    }
     public static void saveGifs(int id, final Activity activity) {
         File fileMain = null;
         try {
             if (!Environment.getExternalStorageState().equals(
                     Environment.MEDIA_MOUNTED)) {
                 Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show();
             } else {
                 fileMain = new File(Environment.getExternalStorageDirectory()
                         + File.separator + "Photo Video Editor" + File.separator + "Video Editor Gifs" + File.separator);
                 boolean success = fileMain.mkdirs();
             }
             File file1 = new File(fileMain, "check" + System.currentTimeMillis() + ".gif");
             try {
                 InputStream is = activity.getResources().openRawResource(id);
                 OutputStream os = new FileOutputStream(file1);
                 byte[] data = new byte[is.available()];
                 is.read(data);
                 os.write(data);
                 is.close();
                 os.close();
             } catch (IOException e) {
                 Toast.makeText(activity, "Exception", Toast.LENGTH_SHORT).show();
             }
         } catch (Exception e1) {
             e1.printStackTrace();}
     }
}
