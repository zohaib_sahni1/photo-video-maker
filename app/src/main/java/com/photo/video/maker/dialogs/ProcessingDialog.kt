package com.photo.video.maker.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.ProgressBar
import android.widget.TextView
import com.photo.video.maker.R

class ProcessingDialog(context: Context?) : Dialog(context) {
    private var mContext: Context
    private lateinit var proggressBar: ProgressBar
    private lateinit var progressTextView: TextView

    init {
        this.mContext = context!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.requestFeature(Window.FEATURE_NO_TITLE)
        window!!.setWindowAnimations(R.style.CustomDialogAnimation)
        setContentView(R.layout.processing_dailog_layout)
        setCancelable(false)
        proggressBar = findViewById(R.id.progressBar)
        proggressBar.getIndeterminateDrawable().setColorFilter(mContext.getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY)
        progressTextView = findViewById(R.id.progressTextView)
    }

    fun dismissIt() {
        try {
            dismiss()
        } catch (e: Exception) {
        }
        if (proggressBar.getVisibility() == View.VISIBLE) {
            proggressBar.setVisibility(View.GONE)
        }
    }

    fun setProgressStatus(progressNew: Int) {
        proggressBar.progress = progressNew
    }

    @SuppressLint("SetTextI18n")
    fun setProgressPercentage(percentage: Int) {
        progressTextView.text = "Processing: " + percentage.toString() + " %"
    }
}