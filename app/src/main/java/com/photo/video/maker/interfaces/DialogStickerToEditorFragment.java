package com.photo.video.maker.interfaces;

public interface DialogStickerToEditorFragment {
    void setPhotoEditorSticker(int position);
    void setIntentCall();
}
