package com.photo.video.maker.fragments;

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.facebook.ads.AdSize
import com.google.android.gms.ads.AdView
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import com.photo.video.maker.R
import com.photo.video.maker.utils.BillingHelper
import com.photo.video.maker.utils.MyConstants
import com.photo.video.maker.utils.MyEssentials


class EditVideoFragment : Fragment(), View.OnClickListener, MediaPlayer.OnCompletionListener,
    MediaPlayer.OnPreparedListener {
    private lateinit var videoView: VideoView
    private lateinit var doneBtn: ImageView
    private var videoPath: String? = null
    private lateinit var mediaController: MediaController
    private lateinit var mContext: Context
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var billingHelper: BillingHelper
    private lateinit var fbAdContainer: LinearLayout
    private lateinit var mAdView: AdView
    private lateinit var fbAdView: com.facebook.ads.AdView
    private lateinit var fbAdContainerNew: LinearLayout
    private lateinit var mAdViewNew: AdView
    private lateinit var fbAdViewNew: com.facebook.ads.AdView
    private lateinit var videoContainer: ConstraintLayout
    private lateinit var adsLayout: ConstraintLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit_video, container, false)
        initViews(view)
        activity?.title = "Upload Video"
        return view;
    }

    fun initViews(view: View) {
        doneBtn = view.findViewById(R.id.doneBtn)
        doneBtn.setOnClickListener(this)
        videoContainer = view.findViewById(R.id.videoContainer)
        adsLayout = view.findViewById(R.id.adsLayout)
        view.findViewById<ImageView>(R.id.backBtn).setOnClickListener(this)
        view.findViewById<CardView>(R.id.uploadBtn).setOnClickListener(this)
        mFragmentManager = activity!!.supportFragmentManager
        videoView = view.findViewById(R.id.videoView)
        mediaController = MediaController(mContext)
        videoView.setOnPreparedListener(this);
        billingHelper = BillingHelper(mContext)
        mAdView = view.findViewById(R.id.adView)
        fbAdView = com.facebook.ads.AdView(mContext, resources.getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50)
        fbAdContainer = view.findViewById(R.id.fbAdContainer)
        mAdViewNew = view.findViewById(R.id.adViewNew)
        fbAdViewNew =
            com.facebook.ads.AdView(mContext, resources.getString(R.string.fb_medium), AdSize.RECTANGLE_HEIGHT_250)
        fbAdContainerNew = view.findViewById(R.id.fbAdContainerNew)
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView)
            MyEssentials.loadFbBannerAd(mContext, fbAdContainerNew, fbAdViewNew, mAdViewNew)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context!!
    }

    override fun onResume() {
        super.onResume()
        if (activity != null) {
            mContext = activity!!
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == MyConstants.VIDEO_PICK_INTENT_KEY) {
                doneBtn.visibility = View.VISIBLE
                adsLayout.visibility = View.GONE
                videoContainer.visibility = View.VISIBLE
                try {
                    videoPath = getVideoPath(data?.data)
                } catch (e: Exception) {
                }
                mediaController.setAnchorView(videoView)
                mediaController.setMediaPlayer(videoView)
                if (videoPath != null) {
                    try {
                        videoView.setVideoURI(Uri.parse(videoPath))
                    } catch (e: Exception) {
                    }
                }
                try {
                    videoView.setMediaController(mediaController)
                } catch (e: Exception) {
                }
                videoView.requestFocus()
                videoView.setOnCompletionListener(this)
                try {
                    videoView.start()
                } catch (e: Exception) {
                }
            }
        }
    }

    override fun onPrepared(mp: MediaPlayer) {
        try {
            if (!activity!!.isFinishing) {
                mediaController.show(100)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getVideoPath(uri: Uri?): String? {
        val projectionForVideoUrl = arrayOf(MediaStore.Images.Media.DATA)
        val cursorVideo = activity!!.managedQuery(uri, projectionForVideoUrl, null, null, null)
        if (cursorVideo != null) {
            val column_index = cursorVideo.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursorVideo.moveToFirst()
            return cursorVideo.getString(column_index)
        } else
            return null
    }

    override fun onCompletion(p0: MediaPlayer?) {
        try {
            videoView.start()
        } catch (e: Exception) {
        }
    }

    override fun onPause() {
        super.onPause()
        videoView.pause()
    }

    override fun onStop() {
        super.onStop()
        videoView.stopPlayback()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.doneBtn -> {
                val videoViewFragment =
                    VideoViewFragment.getInstance(videoPath, MyConstants.VIDEO_VIEW_ENTER_WITH_EDITED)
                mFragmentManager.beginTransaction().replace(R.id.frameLayout, videoViewFragment).addToBackStack(null)
                    .commitAllowingStateLoss()
            }
            R.id.backBtn -> {
                try {
                    mFragmentManager.popBackStack()
                } catch (e: Exception) {
                }
            }
            R.id.uploadBtn -> {
                try {
                    val intent = Intent(Intent.ACTION_PICK)
                    intent.setDataAndType(android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "video/*")
                    startActivityForResult(intent, MyConstants.VIDEO_PICK_INTENT_KEY)
                } catch (e: Exception) {
                }
            }
        }
    }
}
