package com.photo.video.maker.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.Toast;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.SelectedImageAdapter;
import com.photo.video.maker.dialogs.*;
import com.photo.video.maker.interfaces.DialogDurationToSelectedFragment;
import com.photo.video.maker.interfaces.NameDialogToSelectedFragmentInterface;
import com.photo.video.maker.interfaces.SelectedFragmentToAdapterInterface;
import com.photo.video.maker.interfaces.VideoPrviewToFragment;
import com.photo.video.maker.utils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class SelectedImagesFragment extends Fragment implements View.OnClickListener, SelectedFragmentToAdapterInterface, DialogDurationToSelectedFragment, BottomNavigationView.OnNavigationItemSelectedListener, NameDialogToSelectedFragmentInterface, VideoPrviewToFragment {
    RecyclerView recyclerViewSelectedPhotos;
    SelectedImageAdapter adapter;
    ArrayList<String> dataReceived;
    FragmentManager fragmentManager;
    FFmpeg fFmpeg;
    private int durationInSecs = 2;
    String imagesEffect = " ";
    Context mContext;
    ImageView doneBtn;
    int listLength;
    NewProcessingDialog imagesProcessingDialog;
    boolean shouldGoWithAsynch;
    BottomNavigationView bottomNavigationFirst;
    VideoPreviewDialog dialog;
    SlideEncoder slideEncoder;
    String myVideoName;
    ProcessingDialog processingDialog;

    //may be dialog confusion
    public static SelectedImagesFragment getInstance(ArrayList<String> data, boolean shouldDoAsync) {
        Bundle args = new Bundle();
        args.putStringArrayList(MyConstants.SELECTED_IMAGE_KEY, data);
        args.putBoolean(MyConstants.SELECTED_IMAGE_FRAGMENT_ASYNCH, shouldDoAsync);
        SelectedImagesFragment fragment = new SelectedImagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selected_images, container, false);
        initViews(view);
        try {
            loadFffmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initViews(View view) {
        view.findViewById(R.id.backBtn).setOnClickListener(this);
        view.findViewById(R.id.doneBtn).setOnClickListener(this);
        slideEncoder = new SlideEncoder();
        bottomNavigationFirst = view.findViewById(R.id.bottomNavMenu);
        bottomNavigationFirst.setOnNavigationItemSelectedListener(this);
        doneBtn = view.findViewById(R.id.doneBtn);
        view.findViewById(R.id.previewBtn).setOnClickListener(this);
        fragmentManager = getActivity().getSupportFragmentManager();
        recyclerViewSelectedPhotos = view.findViewById(R.id.recyclerViewSelectedPhotos);
        assert getArguments() != null;
        try {
            dataReceived = getArguments().getStringArrayList(MyConstants.SELECTED_IMAGE_KEY);
            shouldGoWithAsynch = getArguments().getBoolean(MyConstants.SELECTED_IMAGE_FRAGMENT_ASYNCH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert dataReceived != null;
        try {
            listLength = dataReceived.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listLength > 0) {
            doneBtn.setVisibility(View.VISIBLE);
        }
        if (dataReceived.size() > 0) {
            adapter = new SelectedImageAdapter(mContext, fragmentManager, dataReceived);
        }
        recyclerViewSelectedPhotos.setLayoutManager(new WrapContentGridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false));
        recyclerViewSelectedPhotos.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelectedPhotos.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callBack);
        itemTouchHelper.attachToRecyclerView(recyclerViewSelectedPhotos);
    }

    ItemTouchHelper.Callback callBack = new ItemTouchHelper.Callback() {
        public boolean onMove(@NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            try {
                Collections.swap(dataReceived, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                //adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                recyclerViewSelectedPhotos.getRecycledViewPool().clear();
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        }

        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                try {
                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.doneBtn:
                if (dataReceived.size() < 3) {
                    Toast.makeText(getActivity(), "Please Select At least three image for making video!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    VideoNameDialog videoNameDialog = new VideoNameDialog(mContext, this);
                    videoNameDialog.show();
                }
                break;
            case R.id.previewBtn:
                Toast.makeText(mContext, "Starting Video Preview", Toast.LENGTH_SHORT).show();
                dialog = new VideoPreviewDialog(mContext, dataReceived, durationInSecs * 1000, this);
                dialog.show();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.duraitonBtn:
                AddDurationDialog durationDialog = new AddDurationDialog(mContext, this);
                durationDialog.show();
                break;
            case R.id.effectBtn:
                dialog = new VideoPreviewDialog(mContext, dataReceived, durationInSecs * 1000, this);
                dialog.show();
                break;
        }
        return true;
    }

    @Override
    public ArrayList<String> getOriginalList() {
        return dataReceived;
    }

    private void loadFffmpeg() throws FFmpegNotSupportedException {
        if (fFmpeg == null) {
            fFmpeg = FFmpeg.getInstance(mContext);
        }
        fFmpeg.loadBinary(new LoadBinaryResponseHandler() {
            @Override
            public void onFailure() {
                super.onFailure();
                Toast.makeText(mContext, "Photo Video Maker Framework is not supported by this device!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                Toast.makeText(mContext, "Photo Video Maker Framework is supported by this device!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void setDuration(int duration) {
        durationInSecs = duration;
    }

    @Override
    public void setEffect(String effect) {
        imagesEffect = effect;
    }

    @SuppressLint("StaticFieldLeak")
    class VideoWithTransitionTask extends AsyncTask<Void, Integer, Void> {
        File videoFileNew;
        private int interval;
        ArrayList<Bitmap> animayBitmapList = new ArrayList<>();
        int currentProgress;

        VideoWithTransitionTask(int interval) {
            if (myVideoName.equalsIgnoreCase("empty")) {
                videoFileNew = SaveUtils.createVideoPath(getActivity());
            } else {
                videoFileNew = SaveUtils.createVideoPathWithMyName(getActivity(), myVideoName);
            }
            this.interval = interval;
            processingDialog = new ProcessingDialog(mContext);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!processingDialog.isShowing()) {
                try {
                    processingDialog.show();
                } catch (Exception e) {
                    Toast.makeText(mContext, "Failed to show processingDialog", Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inDither = false;
            bmOptions.inScaled = true;
            bmOptions.inPreferredConfig = Bitmap.Config.RGB_565;
            bmOptions.inSampleSize = 3;
            for (int i = 0; i < dataReceived.size(); i++) {
                Bitmap oldBitmap = null;
                try {
                    if (dataReceived.get(i) != null) {
                        oldBitmap = BitmapFactory.decodeFile(dataReceived.get(i), bmOptions);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (dataReceived.get(i) != null) {
                        oldBitmap = BitmapFactory.decodeFile(dataReceived.get(i), bmOptions);
                    }
                }
                Bitmap newBitmap = null;
                try {
                    if (oldBitmap != null) {
                        newBitmap = Bitmap.createScaledBitmap(oldBitmap, 512, 512, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (animayBitmapList != null) {
                    try {
                        animayBitmapList.add(newBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (oldBitmap != newBitmap) {
                    try {
                        oldBitmap.recycle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (videoFileNew.exists())
                try {
                    videoFileNew.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            try {
                slideEncoder.prepareEncoder(videoFileNew);
                Bitmap prevBm = null;
                for (int idx = 0; idx < animayBitmapList.size(); idx++) {
                    MySlideShow.Companion.init();
                    if (idx > 0) prevBm = animayBitmapList.get(idx - 1);
                    Bitmap curBm = animayBitmapList.get(idx);
                    for (int i = 0; i < (MySlideApplication.Companion.getFRAME_PER_SEC() * interval); i++) {
                        try {
                            slideEncoder.drainEncoder(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            slideEncoder.generateFrame(prevBm, curBm);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    currentProgress += (int) (1f * (100 / animayBitmapList.size()));
                    publishProgress(currentProgress);
                }
                try {
                    slideEncoder.drainEncoder(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                slideEncoder.releaseEncoder();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            try {
                processingDialog.setProgressStatus(values[0]);
                processingDialog.setProgressPercentage(values[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (processingDialog.isShowing()) {
                    processingDialog.dismiss();
                }
                for (Bitmap bm : animayBitmapList) {
                    if (bm != null) {
                        bm.recycle();
                    }
                }
                try {
                    animayBitmapList.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                VideoViewFragment videoViewFragment = VideoViewFragment.getInstance(videoFileNew.getAbsolutePath(), MyConstants.VIDEO_VIEW_ENTER_WITH_CREATED);
                fragmentManager.beginTransaction().replace(R.id.frameLayout, videoViewFragment).addToBackStack(null).commitAllowingStateLoss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setNameAndGo(String videoName) {
        myVideoName = videoName;
        new VideoWithTransitionTask(durationInSecs).execute();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (imagesProcessingDialog != null) {
            imagesProcessingDialog.dismissIt();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (imagesProcessingDialog != null) {
            imagesProcessingDialog.dismissIt();
        }
        try {
            if (fFmpeg != null) {
                fFmpeg.killRunningProcesses();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
