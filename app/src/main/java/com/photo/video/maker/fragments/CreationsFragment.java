package com.photo.video.maker.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.AnalyticsConstants;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.FbAdListener;
import com.photo.video.maker.utils.MyEssentials;

import java.io.File;

public class CreationsFragment extends Fragment implements View.OnClickListener {
    ImageView imagesBtn, videoBtn;
    Context mContext;
    LinearLayout fbAdContainer;
    InterstitialAd mInterstitialAd;
    com.facebook.ads.InterstitialAd fbInterstitialAd;
    BillingHelper billingHelper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_creations, container, false);
        imagesBtn = view.findViewById(R.id.imagesBtn);
        videoBtn = view.findViewById(R.id.videosBtn);
        imagesBtn.setOnClickListener(this);
        videoBtn.setOnClickListener(this);
        billingHelper = new BillingHelper(getActivity());
        AdView mAdView = view.findViewById(R.id.adView);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(mContext, getResources().getString(R.string.fb_medium), AdSize.RECTANGLE_HEIGHT_250);
        fbAdContainer = view.findViewById(R.id.fbAdContainer);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(mContext, fbAdContainer, fbAdView, mAdView);
            mInterstitialAd = MyEssentials.loadAdMobInterstitialAd(mContext);
            fbInterstitialAd = MyEssentials.loadFbInterstitialAd(mContext);
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(mContext, "Restart the application and grant all the permissions!", Toast.LENGTH_SHORT).show();
        } else {
            creationsItemClicked(view.getId());
        }
    }

    private void creationsItemClicked(int id) {
        switch (id) {
            case R.id.imagesBtn:
                File files = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Photo Video Editor" + File.separator + "Video Editor Images" + File.separator);
                if (files.exists()) {
                    setFragment(new ImagesCreationsFragment());

                } else {
                    Toast.makeText(getActivity(), "No Image available in Images folder", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.videosBtn:
                File files1 = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Photo Video Editor" + File.separator + "Video Editor Videos" + File.separator);
                if (files1.exists()) {
                    setFragment(new VideoCreationsFragment());

                } else {
                    Toast.makeText(getActivity(), "No Video available in videos folder", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void setFragment(final android.support.v4.app.Fragment fragment) {
        if (MyEssentials.isMediation) {
            if (fbInterstitialAd.isAdLoaded() && !fbInterstitialAd.isAdInvalidated()) {
                fbInterstitialAd.show();
                fbInterstitialAd.setAdListener(new FbAdListener() {
                    @Override
                    public void onInterstitialDismissed(Ad ad) {
                        super.onInterstitialDismissed(ad);
                        MyEssentials.isMediation = false;
                        MyEssentials.setTimer(5000);
                        android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                        t.replace(R.id.frameLayout, fragment);
                        t.addToBackStack(null);
                        t.commitAllowingStateLoss();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        super.onAdClicked(ad);
                        MyEssentials.isMediation = false;
                        MyEssentials.setTimer(15000);
                        MyEssentials.logEvents(AnalyticsConstants.CREATIONS_ACTIVITY_BUTTON_CLICKED, getActivity());
                    }
                });
            } else if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        MyEssentials.isMediation = false;
                        MyEssentials.setTimer(5000);
                        android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                        t.replace(R.id.frameLayout, fragment);
                        t.addToBackStack(null);
                        t.commitAllowingStateLoss();
                    }

                    @Override
                    public void onAdClicked() {
                        super.onAdClicked();
                        MyEssentials.isMediation = false;
                        MyEssentials.setTimer(15000);
                        MyEssentials.logEvents(AnalyticsConstants.CREATIONS_ACTIVITY_BUTTON_CLICKED, getActivity());
                    }
                });
            } else {
                android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
                t.replace(R.id.frameLayout, fragment);
                t.addToBackStack(null);
                t.commitAllowingStateLoss();
            }
        } else {
            android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
            t.replace(R.id.frameLayout, fragment);
            t.addToBackStack(null);
            t.commitAllowingStateLoss();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && mContext == null) {
            mContext = getActivity();
        }
    }
}
