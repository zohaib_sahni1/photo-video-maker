package com.photo.video.maker.model;

import java.util.ArrayList;

public class ImagesModel {
    private String str_folder;
    private ArrayList<String> allImagesPaths;

    public String getStr_folder() { return str_folder; }

    public void setStr_folder(String str_folder) {
        this.str_folder = str_folder;
    }

    public ArrayList<String> getAl_imagepath() {
        return allImagesPaths;
    }

    public void setAl_imagepath(ArrayList<String> allImagesPaths) {
        this.allImagesPaths = allImagesPaths;
    }
}
