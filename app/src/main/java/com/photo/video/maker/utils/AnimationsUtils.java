package com.photo.video.maker.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import com.photo.video.maker.R;

public class AnimationsUtils {
    public static void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    public static void setBounceAnimation(View view, Context mContext) {
       Animation anim=AnimationUtils.loadAnimation(mContext,R.anim.bounce_animation);
        view.startAnimation(anim);
    }
    public static void setZoomAnimation(View view, Context mContext) {
        Animation anim=AnimationUtils.loadAnimation(mContext,R.anim.zoom);
        view.startAnimation(anim);
    }
}
