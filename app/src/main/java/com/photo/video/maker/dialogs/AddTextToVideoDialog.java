package com.photo.video.maker.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.FrameAdapter;
import com.photo.video.maker.interfaces.DialogTextToVideoInterface;
import com.photo.video.maker.model.FrameModel;
import com.photo.video.maker.utils.ImageUtils2;
import com.photo.video.maker.utils.MyConstants;
import com.photo.video.maker.utils.RecyclerTouchListener;
import com.photo.video.maker.utils.WrapContentLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;


public class AddTextToVideoDialog extends Dialog implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private Context context;
    private DialogTextToVideoInterface callBack;
    private EditText editText;
    private String position = MyConstants.CENTER_ALLIGN_POSITION;
    private List<FrameModel> frameModelList = new ArrayList<>();
    private int textSize = 30;
    private String colorSelected;
    private int colorSelectedInt;
    private TextView positionViewText;
    private TextView textView2;

    public AddTextToVideoDialog(@NonNull Context context, DialogTextToVideoInterface callBack) {
        super(context);
        this.context = context;
        this.callBack = callBack;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_text_to_video_new_layout);
        editText = findViewById(R.id.editText);
        positionViewText = findViewById(R.id.textView1);
        colorSelected = "White";
        ImageView backBtn = findViewById(R.id.backBtn);
        ImageView textPositionBtn = findViewById(R.id.textPositionBtn);
        textPositionBtn.setOnClickListener(this);
        textView2 = findViewById(R.id.textView2);
        backBtn.setOnClickListener(this);
        findViewById(R.id.doneBtn).setOnClickListener(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        SeekBar slider = findViewById(R.id.slider);
        slider.setOnSeekBarChangeListener(this);
        slider.setProgress(30);
        slider.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.new_text_color), PorterDuff.Mode.SRC_IN);
        slider.getThumb().setColorFilter(context.getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        //LinearLayoutManager frameLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        //recyclerView.setLayoutManager(frameLayoutManager);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        frameModelList = ImageUtils2.updateList(frameModelList);
        FrameAdapter adapter = new FrameAdapter(frameModelList, context);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                colorSelectedInt = frameModelList.get(position).getColor();
                colorSelected = frameModelList.get(position).getColorName();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        editText.setText(" ");
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismissDialog();
                break;
            case R.id.doneBtn:
                if (editText.getText().toString().equals("")) {
                    Toast.makeText(context, "Please enter some Text!", Toast.LENGTH_SHORT).show();
                } else {
                    String text = editText.getText().toString();
                    callBack.setTextToVideo(text, position, colorSelected, colorSelectedInt,textSize, true);
                }
                dismissDialog();
                break;
            case R.id.textPositionBtn:
                final PopupMenu menu = new PopupMenu(context, textView2);
                menu.getMenuInflater().inflate(R.menu.text_video_position_menu, menu.getMenu());
                menu.show();
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.center_align:
                                position = MyConstants.CENTER_ALLIGN_POSITION;
                                positionViewText.setText(context.getString(R.string.center_allign));
                                break;
                            case R.id.center_bottom:
                                position = MyConstants.CENTER_BOTTOM_POSITION;
                                positionViewText.setText(context.getString(R.string.center_bottom));
                                break;
                            case R.id.bottom_right:
                                position = MyConstants.BOTTOM_RIGHT_POSITION;
                                positionViewText.setText(context.getString(R.string.bottom_right));
                                break;
                            case R.id.bottom_left:
                                position = MyConstants.BOTTOM_LEFT_POSITION;
                                positionViewText.setText(context.getString(R.string.bottom_left));
                                break;
                            case R.id.top_right:
                                position = MyConstants.TOP_RIGHT_POSITION;
                                positionViewText.setText(context.getString(R.string.top_right));
                                break;
                            case R.id.top_left:
                                position = MyConstants.TOP_LEFT_POSITION;
                                positionViewText.setText(context.getString(R.string.top_left));
                                break;
                            default:
                                position = MyConstants.CENTER_ALLIGN_POSITION;
                                break;
                        }
                        return true;
                    }
                });
                break;
        }
    }

    private void dismissDialog() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        textSize = i;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
