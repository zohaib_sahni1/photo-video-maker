package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.SlidingAdapterToGridAdapter;

import java.util.ArrayList;

public class SlidingViewAdapter extends RecyclerView.Adapter<SlidingViewAdapter.MyViewHodlerNew> {
    private Context context;
    private ArrayList<String> pathDataList;
    private SlidingAdapterToGridAdapter callBack;

    public SlidingViewAdapter(Context context, ArrayList<String> pathDataList, SlidingAdapterToGridAdapter callBack) {
        this.context = context;
        this.pathDataList = pathDataList;
        this.callBack = callBack;
    }

    public void setPathDataList(ArrayList<String> pathDataList) {
        this.pathDataList = pathDataList;
    }

    @NonNull
    @Override
    public MyViewHodlerNew onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sliding_item_layout, parent, false);
        return new MyViewHodlerNew(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHodlerNew holder, final int position) {
        //Glide.with(context).applyDefaultRequestOptions(new RequestOptions().override(500,500).fitCenter()).load(pathDataList.get(holder.getAdapterPosition())).into(holder.imageView);
        if (position != RecyclerView.NO_POSITION) {
            Glide.with(context).load(pathDataList.get(position)).into(holder.imageView);
            holder.crossBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        callBack.doChanges(true, position, pathDataList.get(position));
                        pathDataList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, pathDataList.size());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }
    @Override
    public int getItemCount() {
        return pathDataList.size();
    }

    class MyViewHodlerNew extends RecyclerView.ViewHolder {
        ImageView imageView, crossBtn;

        MyViewHodlerNew(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            crossBtn = itemView.findViewById(R.id.crossBtn);
        }
    }
}
