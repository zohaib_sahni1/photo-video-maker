package com.photo.video.maker.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.photo.video.maker.R;
import com.photo.video.maker.activities.MainActivity;
import com.photo.video.maker.dialogs.ProcessingDialogNew;
import com.photo.video.maker.fragments.VideoViewWithMusicFragment;


import static android.app.Notification.VISIBILITY_PUBLIC;

public class FFmpegHelper {
    private static boolean isSuccess;
    private static NotificationCompat.Builder builder;
    private static NotificationManagerCompat notificationManagerCompat;
    private static int notificationId = 1;
    private static String notificationChannelId = "10";

    public static void executeFfmpegMusicCommands(final Context mContext, final String[] ffmpegCommand, final FFmpeg fFmpeg, final String destinationPath, final android.support.v4.app.FragmentManager fragmentManager, final ProcessingDialogNew dialog) {
        try {
            fFmpeg.execute(ffmpegCommand, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    isSuccess = true;
                    Toast.makeText(mContext, "Task performed successfully!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onProgress(String message) {
                    dialog.setTextToDialog(message);
                }

                @Override
                public void onFailure(String message) {
                    cancelNotification();
                    try {
                        dialog.dismissIt();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext, "Task is failed with message: " + message, Toast.LENGTH_LONG).show();
                    Log.d("FFMPEG MESSAGE", message);
                }

                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onStart() {
                    dialog.show();
                    showNotification(mContext, "Processing Video...");
                    notificationManagerCompat.notify(notificationId, builder.build());
                }

                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onFinish() {
                    if (isSuccess) {
                        dialog.dismissIt();
                        cancelNotification();
                        showNotification(mContext, "Task performed successfully!");
                        try {
                            Intent toMainIntent = new Intent(mContext, MainActivity.class);
                            toMainIntent.setAction(Intent.ACTION_MAIN);
                            toMainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                            toMainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            toMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            toMainIntent.putExtra(MyConstants.FRAGMENT_SELECTION_KEY, "video_view_with_music_fragment");
                            toMainIntent.putExtra(MyConstants.FRAGMENT_SELECTION_PATH, destinationPath);
                            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, toMainIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                            builder.setContentIntent(pendingIntent);
                            builder.setProgress(0, 0, false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        notificationManagerCompat.notify(notificationId, builder.build());
                        VideoViewWithMusicFragment videoViewFragment = VideoViewWithMusicFragment.getInstance(destinationPath, MyConstants.TO_MUSIC_ACTION_SIMPLE);
                        fragmentManager.beginTransaction().replace(R.id.frameLayout, videoViewFragment).addToBackStack(null).commitAllowingStateLoss();
                    }
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static void showNotification(Context context, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannelId = createNotificationChanel(context);
        }
        builder = new NotificationCompat.Builder(context, notificationChannelId)
                .setSmallIcon(R.mipmap.app_icon)
                .setContentTitle(text)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setAutoCancel(true)
                .setVisibility(VISIBILITY_PUBLIC)
                .setOngoing(false)
                .setSound(null)
                .setProgress(0, 0, true);
        notificationManagerCompat = NotificationManagerCompat.from(context);
    }

    public static void cancelNotification() {
        try {
            notificationManagerCompat.cancel(notificationId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static String createNotificationChanel(Context context) {

        String chanelId = null;
        try {
            chanelId = "";
            String chanelName = "LOADING SERVICE ";
            NotificationChannel channel = new NotificationChannel(chanelId, chanelName, NotificationManager.IMPORTANCE_LOW);
            channel.setLightColor(R.color.white);
            channel.setSound(null, null);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.createNotificationChannel(channel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chanelId;
    }
}
