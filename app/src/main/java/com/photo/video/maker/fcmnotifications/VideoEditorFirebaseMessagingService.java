package com.photo.video.maker.fcmnotifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class VideoEditorFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String title = null;
        String body = null;
        String description = null;
        String icon = null;
        Bitmap bitmap = null;
        try {
            title = remoteMessage.getData().get("title");
            body = remoteMessage.getData().get("app_url");
            description = remoteMessage.getData().get("short_desc");
            icon = remoteMessage.getData().get("icon");
            bitmap = getBitmapImageFromRemoteUri(remoteMessage.getData().get("feature"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bitmap bitIcon = getBitmapImageFromRemoteUri(icon);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(FcmConstants.CHANNEL_ID, FcmConstants.CHANNEL_NAME, importance);
            mChannel.setDescription(FcmConstants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            try {
                assert mNotificationManager != null;
                mNotificationManager.createNotificationChannel(mChannel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        FirebaseNotificationProcedure.getInstance(this).displayFirebasePushNotification(title, body, description, bitmap, bitIcon, this);
    }

    public Bitmap getBitmapImageFromRemoteUri(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = null;
            try {
                input = connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
