package com.photo.video.maker.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.MusicAdapter;
import com.photo.video.maker.interfaces.AddMusicInterface;
import com.photo.video.maker.interfaces.MusicDialogToFetchInterface;
import com.photo.video.maker.model.MusicModel;
import com.photo.video.maker.utils.FetchClass;
import com.photo.video.maker.utils.MyDividerItemDecoration;
import com.photo.video.maker.utils.RecyclerTouchListener;
import com.photo.video.maker.utils.WrapContentLinearLayoutManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class AddMusicDialog extends Dialog implements View.OnClickListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MusicDialogToFetchInterface {
    private Context context;
    private AddMusicInterface callBack;
    private List<MusicModel> musicModelList = new ArrayList<>();
    private ArrayList<String> pathStrings = new ArrayList<>();
    private ArrayList<String> nameStrings = new ArrayList<>();
    private ArrayList<String> titleStrings = new ArrayList<>();
    private MediaPlayer mediaPlayer;
    private ImageButton play;
    private String pathToPlay;
    private boolean isFirstClicked = false;
    private int currentIndex = 0;
    private RecyclerView recyclerView;
    private TextView emptyText;
    private ImageButton back;
    private ImageButton forward;
    @SuppressLint("StaticFieldLeak")
    public static ProgressBar bar;

    public AddMusicDialog(@NonNull Context context, AddMusicInterface callBack) {
        super(context);
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_music_layout);
        initView();
        recyclerView = findViewById(R.id.musicRecyclerView);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(context, MyDividerItemDecoration.VERTICAL_LIST, 15));
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        new FetchPathAndNames().execute();
        MusicAdapter adapter = new MusicAdapter(musicModelList, context);
        recyclerView.setAdapter(adapter);//moved from fetch class end
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                currentIndex = position;
                mediaPlayer.reset();
                isFirstClicked = true;
                play.setBackgroundResource(R.drawable.pasue_new);
                try {
                    pathToPlay = musicModelList.get(position).getPath();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (mediaPlayer.isPlaying()) {
                    try {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                initMediaPlayer();
                if (pathToPlay != null && mediaPlayer != null) {
                    try {
                        mediaPlayer.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Cannot start media player", Toast.LENGTH_SHORT).show();
                    try {
                        bar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void initView() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setLooping(false);
        emptyText = findViewById(R.id.emptyText);
        bar = findViewById(R.id.bar);
        bar.setVisibility(View.VISIBLE);
        play = findViewById(R.id.playPauseBtn);
        play.setColorFilter(context.getResources().getColor(R.color.latest_bg));
        back = findViewById(R.id.playBackBtn);
        forward = findViewById(R.id.playForwardBtn);
        findViewById(R.id.backBtn).setOnClickListener(this);
        findViewById(R.id.doneBtn).setOnClickListener(this);
        play.setOnClickListener(this);
        back.setOnClickListener(this);
        forward.setOnClickListener(this);
        play.setBackgroundResource(R.drawable.play);
    }

    private void getMusicPathAndName(File dir) {
        File[] listFile;
        listFile = dir.listFiles();
        if (listFile != null) {
            for (File aListFile : listFile) {
                if (aListFile.isDirectory()) {
                    getMusicPathAndName(aListFile.getAbsoluteFile());

                } else {
                    String fileName = aListFile.getName().toLowerCase();
                    if (fileName.endsWith(".mp3")) {
                        nameStrings.add(aListFile.getName());
                        pathStrings.add(aListFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            mediaPlayer.stop();
            mediaPlayer.release();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void initMediaPlayer() {
        if (pathToPlay != null && mediaPlayer != null) {
            try {
                mediaPlayer.setDataSource(pathToPlay);
                mediaPlayer.prepare();//prepareASync
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Cannot get path!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.playPauseBtn:
                if (mediaPlayer.isPlaying()) {
                    try {
                        mediaPlayer.pause();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    play.setBackgroundResource(R.drawable.play);
                } else {
                    if (!isFirstClicked) {
                        try {
                            pathToPlay = pathStrings.get(0);
                            initMediaPlayer();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    play.setBackgroundResource(R.drawable.pasue_new);
                    try {
                        if (pathToPlay != null && mediaPlayer != null) {
                            mediaPlayer.start();
                        } else {
                            Toast.makeText(context, "Cannot start media player", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.playBackBtn:
                play.setBackgroundResource(R.drawable.pasue_new);
                if (currentIndex != 0) {
                    currentIndex = currentIndex - 1;
                }
                try {
                    pathToPlay = musicModelList.get(currentIndex).getPath();//crashed 189
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (mediaPlayer.isPlaying()) {
                    try {
                        mediaPlayer.stop();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                try {
                    mediaPlayer.reset();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                initMediaPlayer();
                try {
                    if (pathToPlay != null && mediaPlayer != null) {
                        mediaPlayer.start();
                    } else {
                        Toast.makeText(context, "Cannot start media player", Toast.LENGTH_SHORT).show();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.playForwardBtn:
                play.setBackgroundResource(R.drawable.pasue_new);
                currentIndex = currentIndex + 1;
                if (musicModelList != null && musicModelList.size() >= 0 && currentIndex >= 0) {
                    try {
                        pathToPlay = musicModelList.get(currentIndex).getPath();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                try {
                    mediaPlayer.reset();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                initMediaPlayer();
                try {
                    if (pathToPlay != null && mediaPlayer != null) {
                        mediaPlayer.start();
                    } else {
                        Toast.makeText(context, "Cannot start media player", Toast.LENGTH_SHORT).show();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.backBtn:
                dismissDialog();
                break;
            case R.id.doneBtn:
                callBack.setMusicPathToFfmpeg(pathToPlay, true);
                dismissDialog();
                break;
        }
    }

    private void dismissDialog() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        play.setBackgroundResource(R.drawable.play);
    }

    @Override
    public void setBarVisibility(boolean state) {
        if (state) {
            bar.setVisibility(View.VISIBLE);
        } else {
            try {
                bar.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchPathAndNames extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            getMusicPathAndName(Environment.getExternalStorageDirectory());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pathStrings != null && pathStrings.size() > 0) {
                try {
                    pathToPlay = pathStrings.get(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "No Music available", Toast.LENGTH_SHORT).show();
                emptyText.setVisibility(View.VISIBLE);
                try {
                    bar.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                findViewById(R.id.doneBtn).setVisibility(View.GONE);
                play.setVisibility(View.GONE);
                back.setVisibility(View.GONE);
                forward.setVisibility(View.GONE);

            }
            new FetchClass(musicModelList, pathStrings, nameStrings, titleStrings, recyclerView, context).execute();
        }
    }
}
