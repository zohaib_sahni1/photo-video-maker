package com.photo.video.maker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.photo.video.maker.R;
import com.photo.video.maker.interfaces.DialogTextToEditorFragment;

public class AddTextDialog extends Dialog implements View.OnClickListener {
    private TextView colorBtn;
    private EditText editText;
    private int colorSelected;
    private boolean isColorSelected;
    private Context context;
    private DialogTextToEditorFragment callBack;

    public AddTextDialog(@NonNull Context context, DialogTextToEditorFragment callBack) {
        super(context);
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        setContentView(R.layout.dialog_add_text_layout);
        TextView addTextBtn = findViewById(R.id.addTextBtn);
        colorBtn = findViewById(R.id.colorBtn);
        editText = findViewById(R.id.editText);
        ImageView backBtn = findViewById(R.id.backBtn);
        addTextBtn.setOnClickListener(this);
        colorBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addTextBtn:
                String text = editText.getText().toString();
                if (editText.getText().toString().equals("") || !isColorSelected) {
                    Toast.makeText(context, "Please fill all the required fields", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Ready to go!", Toast.LENGTH_SHORT).show();
                    isColorSelected = false;
                    callBack.setPhotoEditorText(text, colorSelected);
                    try {
                        dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.colorBtn:
                ColorPickerDialogBuilder.with(context).setTitle("Choose car color")
                        .initialColor(context.getResources().getColor(R.color.white))
                        .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {

                            public void onColorSelected(int selectedColor) {

                            }
                        })
                        .setPositiveButton("Ok", new ColorPickerClickListener() {

                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                colorBtn.setBackgroundColor(selectedColor);
                                colorSelected = selectedColor;
                                isColorSelected = true;
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                isColorSelected = false;
                            }
                        })
                        .build()
                        .show();
                break;
            case R.id.backBtn:
                dismiss();
                isColorSelected = false;
                break;
        }
    }
}
