package com.photo.video.maker.interfaces;

public interface NameDialogToSelectedFragmentInterface {
    void setNameAndGo(String videoName);
}
