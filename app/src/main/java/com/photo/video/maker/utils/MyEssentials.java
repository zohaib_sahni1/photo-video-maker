package com.photo.video.maker.utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.photo.video.maker.R;

public class MyEssentials {
    public static boolean isMediation = true;

    public static InterstitialAd loadAdMobInterstitialAd(Context mContext) {
        InterstitialAd mInterstitialAd;
        mInterstitialAd = new InterstitialAd(mContext);
        mInterstitialAd.setAdUnitId(mContext.getResources().getString(R.string.adMob_interstitial_ad_unit_id));
        try {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mInterstitialAd;
    }

    public static com.facebook.ads.InterstitialAd loadFbInterstitialAd(Context mContext) {
        com.facebook.ads.InterstitialAd fbInterstitialAd = new com.facebook.ads.InterstitialAd(mContext, mContext.getResources().getString(R.string.fb_interstitial_ad));
        try {
            fbInterstitialAd.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fbInterstitialAd;
    }

    private static void loadAdMobBannerAd(final AdView mAdView, final LinearLayout fbAdContainer) {
        try {
            mAdView.loadAd(new AdRequest.Builder().build());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                fbAdContainer.setVisibility(View.GONE);
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);

                Log.d("Adsss", i + "");
            }
        });
    }

    public static void loadFbBannerAd(Context mContext, final LinearLayout fbAdContainer, final com.facebook.ads.AdView fbAdView, final AdView mAdView) {
        try {
            fbAdView.loadAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fbAdContainer.addView(fbAdView);
        fbAdView.setAdListener(new com.facebook.ads.AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                fbAdView.destroy();
                Log.d("Adsss", adError.getErrorMessage());
                loadAdMobBannerAd(mAdView, fbAdContainer);
            }

            @Override
            public void onAdLoaded(Ad ad) {
                mAdView.destroy();
                if (mAdView.getVisibility() == View.VISIBLE) {
                    mAdView.setVisibility(View.GONE);
                }
                fbAdContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    public static void logEvents(String value, Context context) {
        FirebaseAnalytics firebaseAnalytics;
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle bundle = new Bundle();
        try {
            firebaseAnalytics.logEvent(value, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setTimer(int timer) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isMediation = true;
            }
        }, timer);
    }
}
