package com.photo.video.maker.model;

public class FrameModel {
    private int color;
    private String text;
    private String colorName;

    public FrameModel(int color, String text, String colorName) {
        this.color = color;
        this.text = text;
        this.colorName = colorName;
    }

    public String getColorName() { return colorName;
    }
    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
