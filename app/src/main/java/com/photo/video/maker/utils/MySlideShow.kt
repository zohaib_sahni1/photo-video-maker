package com.photo.video.maker.utils

import android.graphics.*

class MySlideShow {
    companion object {
        val FADE_IN = 0
        val ROTATE = 1
        val SLIDE_IN = 2
        val FADE_OUT = 3
        val NONE = 4

        private var curStartX: Int = 0
        private var curStartY: Int = 0
        private var prevStartX: Int = 0
        private var prevStartY: Int = 0

        private var in_alpha = 0f
        private var rotate = 0f
        private var slideX = SlideEncoder.WIDTH
        private var slideCount = 1
        private var out_alpha = 255f

        fun draw(canvas: Canvas?, prevBm: Bitmap?, curBm: Bitmap?) {
            if (canvas == null || curBm == null) return

            setLocation(prevBm, curBm)

            if (MySlideApplication.SLIDE_EFFECT === ROTATE && prevBm == null)
                canvas.drawColor(Color.BLACK)

            if (prevBm != null)
                drawFadeOut(canvas, prevBm)

            when (MySlideApplication.SLIDE_EFFECT) {
                NONE -> drawNone(canvas, curBm)
                FADE_IN -> drawFadeIn(canvas, curBm)
                ROTATE -> drawRotate(canvas, curBm)
                SLIDE_IN -> drawSlideIn(canvas, curBm)
                FADE_OUT -> drawFadeOut(canvas, curBm)
                else -> throw IllegalStateException("unexpected state")
            }
        }

        private fun setLocation(prevBm: Bitmap?, curBm: Bitmap?) {
            if (curBm != null) {
                val cWidth = curBm.width
                val cHeight = curBm.height

                if (cWidth > cHeight) {
                    curStartX = 0
                    try {
                        curStartY = (SlideEncoder.HEIGHT - cHeight) / 2
                    } catch (e: Exception) {
                    }
                } else if (cHeight > cWidth) {
                    try {
                        curStartX = (SlideEncoder.WIDTH - cWidth) / 2
                    } catch (e: Exception) {
                    }
                    curStartY = 0
                } else {
                    curStartX = 0
                    curStartY = 0
                }
            }

            if (prevBm != null) {
                val pWidth = prevBm.width
                val pHeight = prevBm.height
                if (pWidth > pHeight) {
                    prevStartX = 0
                    prevStartY = (SlideEncoder.HEIGHT - pHeight) / 2
                } else if (pHeight > pWidth) {
                    prevStartX = (SlideEncoder.WIDTH - pWidth) / 2
                    prevStartY = 0
                } else {
                    prevStartX = 0
                    prevStartY = 0
                }
            }
        }

        fun init() {
            in_alpha = 0f
            out_alpha = 255f
            rotate = 0f
            slideX = 800
            slideCount = 1
            curStartX = 0
            curStartY = 0
            prevStartX = 0
            prevStartY = 0
        }

        private fun drawNone(c: Canvas, bm: Bitmap) {
            try {
                c.drawBitmap(bm, curStartX.toFloat(), curStartY.toFloat(), null)
            } catch (e: Exception) {
            }
        }


        private fun drawFadeIn(c: Canvas, bm: Bitmap) {
            val p = Paint()
            val ratio = Math.ceil((255 / MySlideApplication.FRAME_PER_SEC).toDouble()).toInt()
            in_alpha += ratio.toFloat()
            if (in_alpha > 255f) in_alpha = 255f
            p.alpha = in_alpha.toInt()
            try {
                c.drawBitmap(bm, curStartX.toFloat(), curStartY.toFloat(), p)
            } catch (e: Exception) {
            }
        }

        private fun drawFadeOut(c: Canvas, bm: Bitmap) {
            c.drawColor(Color.BLACK)
            val p = Paint()
            val ratio = Math.ceil((255 / MySlideApplication.FRAME_PER_SEC).toDouble()).toInt()
            out_alpha -= ratio.toFloat()
            if (out_alpha < 0f) out_alpha = 0f
            p.alpha = out_alpha.toInt()
            try {
                c.drawBitmap(bm, prevStartX.toFloat(), prevStartY.toFloat(), p)
            } catch (e: Exception) {
            }
        }

        private fun drawRotate(c: Canvas, bm: Bitmap) {
            val matrix = Matrix()
            matrix.preTranslate(curStartX.toFloat(), curStartY.toFloat())
            val ratio = 360 / MySlideApplication.FRAME_PER_SEC
            rotate += Math.ceil(ratio.toDouble()).toFloat()
            if (rotate > 360) rotate = 360f
            matrix.postRotate(rotate, (SlideEncoder.WIDTH / 2).toFloat(), (SlideEncoder.HEIGHT / 2).toFloat())
            try {
                c.drawBitmap(bm, matrix, null)
            } catch (e: Exception) {
            }
        }

        private fun drawSlideIn(c: Canvas, bm: Bitmap) {
            val matrix = Matrix()
            var ratio = 1
            if (slideCount < 30) ratio = Math.pow(slideCount++.toDouble(), 1.4).toInt()
            slideX -= ratio
            if (slideX < curStartX) slideX = curStartX
            matrix.setTranslate(slideX.toFloat(), curStartY.toFloat())
            try {
                c.drawBitmap(bm, matrix, null)
            } catch (e: Exception) {
            }

        }
    }
}