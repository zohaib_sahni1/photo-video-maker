package com.photo.video.maker.model;

public class FilterModel {
private int imageDrawable;
private String text;

    public FilterModel(int imageDrawable, String text) {
        this.imageDrawable = imageDrawable;
        this.text = text;
    }

    public int getImageDrawable() {
        return imageDrawable;
    }


    public String getString() {
        return text;
    }

    public void setString(String text) {
        this.text = text;
    }
}
