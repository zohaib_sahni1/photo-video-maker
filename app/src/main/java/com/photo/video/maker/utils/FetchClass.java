package com.photo.video.maker.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.photo.video.maker.dialogs.AddMusicDialog;
import com.photo.video.maker.model.MusicModel;

import java.util.ArrayList;
import java.util.List;


public class FetchClass extends AsyncTask<Void, Void, ArrayList<String>> {
    private List<MusicModel> musicModelList = new ArrayList<>();
    private ArrayList<String> pathStrings = new ArrayList<>();
    private ArrayList<String> nameStrings = new ArrayList<>();
    private ArrayList<String> titleStrings = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public RecyclerView recyclerView;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Long dur = 0L;
    private MediaMetadataRetriever metaRetriever;

    public FetchClass(List<MusicModel> musicModelList, ArrayList<String> pathStrings, ArrayList<String> nameStrings, ArrayList<String> titleStrings, RecyclerView recyclerView, Context context) {
        this.musicModelList = musicModelList;
        this.pathStrings = pathStrings;
        this.nameStrings = nameStrings;
        this.titleStrings = titleStrings;
        this.recyclerView = recyclerView;
        this.context = context;
        metaRetriever = new MediaMetadataRetriever();
    }

    @Override
    protected ArrayList<String> doInBackground(Void... voids) {
        ArrayList<String> titles = new ArrayList<>();
        for (int i = 0; i < pathStrings.size(); i++) {
            try {
                metaRetriever.setDataSource(pathStrings.get(i));
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            String duration = null;
            try {
                duration = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                //assert duration != null;
                if (duration != null) {
                    dur = Long.parseLong(duration);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            try {
                if (dur != null && duration != null) {
                    String seconds = String.valueOf((dur % 60000) / 1000);
                    String minutes = String.valueOf(dur / 60000);
                    if (seconds.length() == 1) {
                        titles.add("0" + minutes + ":0" + seconds + " Seconds");
                    } else {
                        titles.add("0" + minutes + ":" + seconds + " Seconds");
                    }
                } else {
                    //Toast.makeText(context, "Cannot get duration!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            metaRetriever.release();
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return titles;
    }

    @Override
    protected void onPostExecute(ArrayList<String> list) {
        super.onPostExecute(list);
        titleStrings = list;
        if (nameStrings.size() > 0 && pathStrings.size() > 0 && titleStrings.size() > 0) {
            for (int i = 0; i < pathStrings.size(); i++) {
                try {
                    musicModelList.add(new MusicModel(nameStrings.get(i), titleStrings.get(i), pathStrings.get(i)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                AddMusicDialog.bar.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Toast.makeText(context, "Couldn't get music paths!", Toast.LENGTH_SHORT).show();
        }
    }


}
