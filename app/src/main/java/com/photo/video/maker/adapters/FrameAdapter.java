package com.photo.video.maker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.photo.video.maker.R;
import com.photo.video.maker.model.FrameModel;
import com.photo.video.maker.utils.AnimationsUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

public class FrameAdapter extends RecyclerView.Adapter<FrameAdapter.MyViewHolder> {
    private List<FrameModel> frameModelList;
    Context context;
    private int lastPosition = 0;

    public FrameAdapter(List<FrameModel> frameModelList, Context context) {
        this.frameModelList = frameModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frame_row_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            FrameModel model = frameModelList.get(position);
            holder.imageView.setImageResource(model.getColor());
            if (holder.getAdapterPosition() == lastPosition) {
                holder.tickBtn.setVisibility(View.VISIBLE);
            } else {
                holder.tickBtn.setVisibility(View.GONE);
            }
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastPosition = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return frameModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imageView;
        private ImageView tickBtn;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.frameView);
            tickBtn = itemView.findViewById(R.id.tickBtn);
            AnimationsUtils.setScaleAnimation(itemView);
        }
    }
}
