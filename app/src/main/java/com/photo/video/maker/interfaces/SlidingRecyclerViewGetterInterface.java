package com.photo.video.maker.interfaces;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public interface SlidingRecyclerViewGetterInterface {
    RecyclerView getSlidingRecyclerView();

    void setDataList(ArrayList<String> data);

    ArrayList<String> getDataList();
}
