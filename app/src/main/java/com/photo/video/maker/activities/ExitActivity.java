package com.photo.video.maker.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import com.facebook.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.photo.video.maker.R;
import com.photo.video.maker.utils.BillingHelper;
import com.photo.video.maker.utils.MyConstants;
import com.photo.video.maker.utils.MyEssentials;

public class ExitActivity extends AppCompatActivity {
    BillingHelper billingHelper;
    LinearLayout adContainer;
    LinearLayout adContainer1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit);
        billingHelper = new BillingHelper(this);
        AdView mAdView = findViewById(R.id.adView);
        AdView mAdView1 = findViewById(R.id.adViewNew);
        com.facebook.ads.AdView fbAdView = new com.facebook.ads.AdView(this, this.getString(R.string.fb_medium), AdSize.RECTANGLE_HEIGHT_250);
        com.facebook.ads.AdView fbAdView1 = new com.facebook.ads.AdView(this, this.getString(R.string.fb_banner), AdSize.BANNER_HEIGHT_50);
        adContainer = findViewById(R.id.fbAdContainer);
        adContainer1 = findViewById(R.id.fbAdContainer1);
        if (billingHelper.shouldShowAds()) {
            MyEssentials.loadFbBannerAd(this, adContainer, fbAdView, mAdView);
            MyEssentials.loadFbBannerAd(this, adContainer1, fbAdView1, mAdView1);
        }
        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToMain();
                finish();

            }
        });
        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.rate_us)));
                startActivity(goToMarket);
            }
        });
    }

    @Override
    public void onBackPressed() {
        backToMain();
        super.onBackPressed();
    }

    private void backToMain() {

        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(MyConstants.FRAGMENT_SELECTION_KEY, "main_view_fragment");
        i.putExtra(MyConstants.FRAGMENT_SELECTION_PATH, "No Path");
        startActivity(i);

    }
}
