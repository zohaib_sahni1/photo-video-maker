package com.photo.video.maker.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ja.burhanrashid52.photoeditor.PhotoEditor;

public class SaveUtils {
    public static File saveEditedImage(File fileMain, final Activity activity, PhotoEditor photoEditor) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show();
        } else {
            fileMain = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Photo Video Editor" + File.separator + "Video Editor Images" + File.separator);
            boolean success = fileMain.mkdirs();
        }
        File file1 = new File(fileMain, "EditedImage" + System.currentTimeMillis() + ".png");//PNG size issue
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(activity, " ", Toast.LENGTH_SHORT).show();
        }
        photoEditor.saveAsFile(file1.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
            @Override
            public void onSuccess(@NonNull String imagePath) {
                Toast.makeText(activity, "Image saved to:\n " + imagePath, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(activity, "Failed exception:" + exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        return file1;
    }

    public static File createVideoPath(final Activity activity) {
        File fileMain = null;
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(activity, "No Storage Found", Toast.LENGTH_SHORT).show();
        } else {
            fileMain = new File(Environment.getExternalStorageDirectory() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Videos" + File.separator);
            boolean isSuccess = fileMain.mkdirs();
        }
        File videoFile = new File(fileMain, "Video " + System.currentTimeMillis() + ".mp4");
        return videoFile;
    }

    public static File createVideoPathWithMyName(final Activity activity, String videoName) {
        File fileMain = null;
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(activity, "No Storage Found", Toast.LENGTH_SHORT).show();
        } else {
            fileMain = new File(Environment.getExternalStorageDirectory() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Videos" + File.separator);
            boolean isSuccess = fileMain.mkdirs();
        }
        File videoFile = new File(fileMain, videoName + ".mp4");
        return videoFile;
    }

    private static String generateString(ArrayList<String> pathList, int duration) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < pathList.size(); i++) {
            string.append("file ").append("'").append(pathList.get(i)).append("'").append("\n").append("duration ").append(duration).append("\n");
        }
        return string.toString();
    }

    public static File saveImagesPathToTextFile(ArrayList<String> list, int duration) {
        String data = generateString(list, duration);
        File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Photo Video Editor" + File.separator + "Video Editor Text Files" + File.separator);
        boolean dirCreated = root.exists();
        if (!dirCreated) {
            dirCreated = root.mkdirs();
        }
        if (dirCreated) {
            File textFile = new File(root, "TextFileImages.txt");
            try {
                FileWriter writer = new FileWriter(textFile, false);
                writer.append(data);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return textFile;
        }
        return null;
    }

    private static File createMusicPath(final Activity activity) {
        File fileMain = null;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show();
        } else {
            fileMain = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Photo Video Editor" + File.separator + "Video Editor Musics" + File.separator);
            boolean success = fileMain.mkdirs();
        }
        File file1 = new File(fileMain, "Music" + System.currentTimeMillis() + ".mp3");
        return file1;
    }

    public static void saveBitmap(Bitmap bitmap, Activity activity) {
        Uri uri = null;
        File files = null;
        File file1 = null;
        try {
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(activity, "No storage found", Toast.LENGTH_SHORT).show();
            } else {
                files = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Photo Video Editor" + File.separator + "Video Temp Images" + File.separator);

                boolean success = files.mkdirs();
            }
            file1 = new File(files, "TempImage " + System.currentTimeMillis() + ".PNG");//when gallery empty then reset counter
            FileOutputStream output = new FileOutputStream(file1);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.flush();
            output.close();
            Toast.makeText(activity, "Saved", Toast.LENGTH_SHORT).show();

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static long getFileDuration(File file) {
        long time = 0;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
            retriever.setDataSource(inputStream.getFD());
            time = Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return time / 1000;
    }
}
