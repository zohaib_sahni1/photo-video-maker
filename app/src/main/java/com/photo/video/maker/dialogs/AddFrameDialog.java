package com.photo.video.maker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import com.photo.video.maker.R;
import com.photo.video.maker.adapters.FrameAdapter;
import com.photo.video.maker.interfaces.AddFrameInterface;
import com.photo.video.maker.model.FrameModel;
import com.photo.video.maker.utils.ImageUtils2;
import com.photo.video.maker.utils.RecyclerTouchListener;
import com.photo.video.maker.utils.WrapContentLinearLayoutManager;


import java.util.ArrayList;
import java.util.List;


public class AddFrameDialog extends Dialog implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private Context context;
    private AddFrameInterface callBack;
    private LinearLayout frame;
    private List<FrameModel> frameModelList = new ArrayList<>();
    private String colorSelected;
    private int thickness = 50;
    private int colorSelectedInt;

    public AddFrameDialog(@NonNull Context context, AddFrameInterface callBack) {
        super(context);
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_frame_new_layout);
        frame = findViewById(R.id.coloredFrame);
        findViewById(R.id.doneBtn).setOnClickListener(this);
        findViewById(R.id.backBtn).setOnClickListener(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        colorSelected = "AliceBlue";
        SeekBar slider = findViewById(R.id.slider);
        slider.setProgress(50);
        slider.setOnSeekBarChangeListener(this);
        slider.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.new_text_color), PorterDuff.Mode.SRC_IN);
        slider.getThumb().setColorFilter(context.getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        //LinearLayoutManager frameLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        //recyclerView.setLayoutManager(frameLayoutManager);
        recyclerView.setLayoutManager(new WrapContentLinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        frameModelList = ImageUtils2.updateList(frameModelList);
        FrameAdapter adapter = new FrameAdapter(frameModelList, context);
        try {
            recyclerView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                colorSelected = frameModelList.get(position).getColorName();
                colorSelectedInt = frameModelList.get(position).getColor();
                frame.setBackgroundResource(frameModelList.get(position).getColor());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                dismiss();
                break;
            case R.id.doneBtn:
                callBack.setFrameColor(colorSelected, thickness, colorSelectedInt,true);
                dismiss();
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        thickness = i;
        float scale = context.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (thickness / 2 * scale + 0.5f);
        frame.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
